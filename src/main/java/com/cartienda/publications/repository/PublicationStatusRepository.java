package com.cartienda.publications.repository;

import com.cartienda.publications.domain.PublicationStatus;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PublicationStatus entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PublicationStatusRepository extends JpaRepository<PublicationStatus,Long> {
    
}
