package com.cartienda.publications.repository;

import com.cartienda.publications.domain.Publications;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Publications entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PublicationsRepository extends JpaRepository<Publications,Long> {
    
}
