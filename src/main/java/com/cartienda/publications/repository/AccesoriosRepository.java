package com.cartienda.publications.repository;

import com.cartienda.publications.domain.Accesorios;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Accesorios entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AccesoriosRepository extends JpaRepository<Accesorios,Long> {
    
}
