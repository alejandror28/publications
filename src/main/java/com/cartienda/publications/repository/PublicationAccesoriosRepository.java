package com.cartienda.publications.repository;

import com.cartienda.publications.domain.PublicationAccesorios;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PublicationAccesorios entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PublicationAccesoriosRepository extends JpaRepository<PublicationAccesorios,Long> {
    
}
