package com.cartienda.publications.repository;

import com.cartienda.publications.domain.Templates;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Templates entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TemplatesRepository extends JpaRepository<Templates,Long> {
    
}
