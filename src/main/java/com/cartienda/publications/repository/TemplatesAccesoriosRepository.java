package com.cartienda.publications.repository;

import com.cartienda.publications.domain.TemplatesAccesorios;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TemplatesAccesorios entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TemplatesAccesoriosRepository extends JpaRepository<TemplatesAccesorios,Long> {
    
}
