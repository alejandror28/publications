/**
 * View Models used by Spring MVC REST controllers.
 */
package com.cartienda.publications.web.rest.vm;
