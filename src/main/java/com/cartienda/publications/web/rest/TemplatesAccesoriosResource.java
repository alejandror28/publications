package com.cartienda.publications.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.cartienda.publications.domain.TemplatesAccesorios;

import com.cartienda.publications.repository.TemplatesAccesoriosRepository;
import com.cartienda.publications.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TemplatesAccesorios.
 */
@RestController
@RequestMapping("/api")
public class TemplatesAccesoriosResource {

    private final Logger log = LoggerFactory.getLogger(TemplatesAccesoriosResource.class);

    private static final String ENTITY_NAME = "templatesAccesorios";

    private final TemplatesAccesoriosRepository templatesAccesoriosRepository;

    public TemplatesAccesoriosResource(TemplatesAccesoriosRepository templatesAccesoriosRepository) {
        this.templatesAccesoriosRepository = templatesAccesoriosRepository;
    }

    /**
     * POST  /templates-accesorios : Create a new templatesAccesorios.
     *
     * @param templatesAccesorios the templatesAccesorios to create
     * @return the ResponseEntity with status 201 (Created) and with body the new templatesAccesorios, or with status 400 (Bad Request) if the templatesAccesorios has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/templates-accesorios")
    @Timed
    public ResponseEntity<TemplatesAccesorios> createTemplatesAccesorios(@RequestBody TemplatesAccesorios templatesAccesorios) throws URISyntaxException {
        log.debug("REST request to save TemplatesAccesorios : {}", templatesAccesorios);
        if (templatesAccesorios.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new templatesAccesorios cannot already have an ID")).body(null);
        }
        TemplatesAccesorios result = templatesAccesoriosRepository.save(templatesAccesorios);
        return ResponseEntity.created(new URI("/api/templates-accesorios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /templates-accesorios : Updates an existing templatesAccesorios.
     *
     * @param templatesAccesorios the templatesAccesorios to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated templatesAccesorios,
     * or with status 400 (Bad Request) if the templatesAccesorios is not valid,
     * or with status 500 (Internal Server Error) if the templatesAccesorios couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/templates-accesorios")
    @Timed
    public ResponseEntity<TemplatesAccesorios> updateTemplatesAccesorios(@RequestBody TemplatesAccesorios templatesAccesorios) throws URISyntaxException {
        log.debug("REST request to update TemplatesAccesorios : {}", templatesAccesorios);
        if (templatesAccesorios.getId() == null) {
            return createTemplatesAccesorios(templatesAccesorios);
        }
        TemplatesAccesorios result = templatesAccesoriosRepository.save(templatesAccesorios);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, templatesAccesorios.getId().toString()))
            .body(result);
    }

    /**
     * GET  /templates-accesorios : get all the templatesAccesorios.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of templatesAccesorios in body
     */
    @GetMapping("/templates-accesorios")
    @Timed
    public List<TemplatesAccesorios> getAllTemplatesAccesorios() {
        log.debug("REST request to get all TemplatesAccesorios");
        return templatesAccesoriosRepository.findAll();
    }

    /**
     * GET  /templates-accesorios/:id : get the "id" templatesAccesorios.
     *
     * @param id the id of the templatesAccesorios to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the templatesAccesorios, or with status 404 (Not Found)
     */
    @GetMapping("/templates-accesorios/{id}")
    @Timed
    public ResponseEntity<TemplatesAccesorios> getTemplatesAccesorios(@PathVariable Long id) {
        log.debug("REST request to get TemplatesAccesorios : {}", id);
        TemplatesAccesorios templatesAccesorios = templatesAccesoriosRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(templatesAccesorios));
    }

    /**
     * DELETE  /templates-accesorios/:id : delete the "id" templatesAccesorios.
     *
     * @param id the id of the templatesAccesorios to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/templates-accesorios/{id}")
    @Timed
    public ResponseEntity<Void> deleteTemplatesAccesorios(@PathVariable Long id) {
        log.debug("REST request to delete TemplatesAccesorios : {}", id);
        templatesAccesoriosRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
