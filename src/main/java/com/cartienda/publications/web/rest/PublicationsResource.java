package com.cartienda.publications.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.cartienda.publications.domain.Publications;

import com.cartienda.publications.repository.PublicationsRepository;
import com.cartienda.publications.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Publications.
 */
@RestController
@RequestMapping("/api")
public class PublicationsResource {

    private final Logger log = LoggerFactory.getLogger(PublicationsResource.class);

    private static final String ENTITY_NAME = "publications";

    private final PublicationsRepository publicationsRepository;

    public PublicationsResource(PublicationsRepository publicationsRepository) {
        this.publicationsRepository = publicationsRepository;
    }

    /**
     * POST  /publications : Create a new publications.
     *
     * @param publications the publications to create
     * @return the ResponseEntity with status 201 (Created) and with body the new publications, or with status 400 (Bad Request) if the publications has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/publications")
    @Timed
    public ResponseEntity<Publications> createPublications(@Valid @RequestBody Publications publications) throws URISyntaxException {
        log.debug("REST request to save Publications : {}", publications);
        if (publications.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new publications cannot already have an ID")).body(null);
        }
        Publications result = publicationsRepository.save(publications);
        return ResponseEntity.created(new URI("/api/publications/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /publications : Updates an existing publications.
     *
     * @param publications the publications to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated publications,
     * or with status 400 (Bad Request) if the publications is not valid,
     * or with status 500 (Internal Server Error) if the publications couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/publications")
    @Timed
    public ResponseEntity<Publications> updatePublications(@Valid @RequestBody Publications publications) throws URISyntaxException {
        log.debug("REST request to update Publications : {}", publications);
        if (publications.getId() == null) {
            return createPublications(publications);
        }
        Publications result = publicationsRepository.save(publications);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, publications.getId().toString()))
            .body(result);
    }

    /**
     * GET  /publications : get all the publications.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of publications in body
     */
    @GetMapping("/publications")
    @Timed
    public List<Publications> getAllPublications() {
        log.debug("REST request to get all Publications");
        return publicationsRepository.findAll();
    }

    /**
     * GET  /publications/:id : get the "id" publications.
     *
     * @param id the id of the publications to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the publications, or with status 404 (Not Found)
     */
    @GetMapping("/publications/{id}")
    @Timed
    public ResponseEntity<Publications> getPublications(@PathVariable Long id) {
        log.debug("REST request to get Publications : {}", id);
        Publications publications = publicationsRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(publications));
    }

    /**
     * DELETE  /publications/:id : delete the "id" publications.
     *
     * @param id the id of the publications to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/publications/{id}")
    @Timed
    public ResponseEntity<Void> deletePublications(@PathVariable Long id) {
        log.debug("REST request to delete Publications : {}", id);
        publicationsRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
