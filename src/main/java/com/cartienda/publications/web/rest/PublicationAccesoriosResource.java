package com.cartienda.publications.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.cartienda.publications.domain.PublicationAccesorios;

import com.cartienda.publications.repository.PublicationAccesoriosRepository;
import com.cartienda.publications.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PublicationAccesorios.
 */
@RestController
@RequestMapping("/api")
public class PublicationAccesoriosResource {

    private final Logger log = LoggerFactory.getLogger(PublicationAccesoriosResource.class);

    private static final String ENTITY_NAME = "publicationAccesorios";

    private final PublicationAccesoriosRepository publicationAccesoriosRepository;

    public PublicationAccesoriosResource(PublicationAccesoriosRepository publicationAccesoriosRepository) {
        this.publicationAccesoriosRepository = publicationAccesoriosRepository;
    }

    /**
     * POST  /publication-accesorios : Create a new publicationAccesorios.
     *
     * @param publicationAccesorios the publicationAccesorios to create
     * @return the ResponseEntity with status 201 (Created) and with body the new publicationAccesorios, or with status 400 (Bad Request) if the publicationAccesorios has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/publication-accesorios")
    @Timed
    public ResponseEntity<PublicationAccesorios> createPublicationAccesorios(@RequestBody PublicationAccesorios publicationAccesorios) throws URISyntaxException {
        log.debug("REST request to save PublicationAccesorios : {}", publicationAccesorios);
        if (publicationAccesorios.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new publicationAccesorios cannot already have an ID")).body(null);
        }
        PublicationAccesorios result = publicationAccesoriosRepository.save(publicationAccesorios);
        return ResponseEntity.created(new URI("/api/publication-accesorios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /publication-accesorios : Updates an existing publicationAccesorios.
     *
     * @param publicationAccesorios the publicationAccesorios to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated publicationAccesorios,
     * or with status 400 (Bad Request) if the publicationAccesorios is not valid,
     * or with status 500 (Internal Server Error) if the publicationAccesorios couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/publication-accesorios")
    @Timed
    public ResponseEntity<PublicationAccesorios> updatePublicationAccesorios(@RequestBody PublicationAccesorios publicationAccesorios) throws URISyntaxException {
        log.debug("REST request to update PublicationAccesorios : {}", publicationAccesorios);
        if (publicationAccesorios.getId() == null) {
            return createPublicationAccesorios(publicationAccesorios);
        }
        PublicationAccesorios result = publicationAccesoriosRepository.save(publicationAccesorios);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, publicationAccesorios.getId().toString()))
            .body(result);
    }

    /**
     * GET  /publication-accesorios : get all the publicationAccesorios.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of publicationAccesorios in body
     */
    @GetMapping("/publication-accesorios")
    @Timed
    public List<PublicationAccesorios> getAllPublicationAccesorios() {
        log.debug("REST request to get all PublicationAccesorios");
        return publicationAccesoriosRepository.findAll();
    }

    /**
     * GET  /publication-accesorios/:id : get the "id" publicationAccesorios.
     *
     * @param id the id of the publicationAccesorios to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the publicationAccesorios, or with status 404 (Not Found)
     */
    @GetMapping("/publication-accesorios/{id}")
    @Timed
    public ResponseEntity<PublicationAccesorios> getPublicationAccesorios(@PathVariable Long id) {
        log.debug("REST request to get PublicationAccesorios : {}", id);
        PublicationAccesorios publicationAccesorios = publicationAccesoriosRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(publicationAccesorios));
    }

    /**
     * DELETE  /publication-accesorios/:id : delete the "id" publicationAccesorios.
     *
     * @param id the id of the publicationAccesorios to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/publication-accesorios/{id}")
    @Timed
    public ResponseEntity<Void> deletePublicationAccesorios(@PathVariable Long id) {
        log.debug("REST request to delete PublicationAccesorios : {}", id);
        publicationAccesoriosRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
