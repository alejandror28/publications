package com.cartienda.publications.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.cartienda.publications.domain.Accesorios;

import com.cartienda.publications.repository.AccesoriosRepository;
import com.cartienda.publications.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Accesorios.
 */
@RestController
@RequestMapping("/api")
public class AccesoriosResource {

    private final Logger log = LoggerFactory.getLogger(AccesoriosResource.class);

    private static final String ENTITY_NAME = "accesorios";

    private final AccesoriosRepository accesoriosRepository;

    public AccesoriosResource(AccesoriosRepository accesoriosRepository) {
        this.accesoriosRepository = accesoriosRepository;
    }

    /**
     * POST  /accesorios : Create a new accesorios.
     *
     * @param accesorios the accesorios to create
     * @return the ResponseEntity with status 201 (Created) and with body the new accesorios, or with status 400 (Bad Request) if the accesorios has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/accesorios")
    @Timed
    public ResponseEntity<Accesorios> createAccesorios(@Valid @RequestBody Accesorios accesorios) throws URISyntaxException {
        log.debug("REST request to save Accesorios : {}", accesorios);
        if (accesorios.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new accesorios cannot already have an ID")).body(null);
        }
        Accesorios result = accesoriosRepository.save(accesorios);
        return ResponseEntity.created(new URI("/api/accesorios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /accesorios : Updates an existing accesorios.
     *
     * @param accesorios the accesorios to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated accesorios,
     * or with status 400 (Bad Request) if the accesorios is not valid,
     * or with status 500 (Internal Server Error) if the accesorios couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/accesorios")
    @Timed
    public ResponseEntity<Accesorios> updateAccesorios(@Valid @RequestBody Accesorios accesorios) throws URISyntaxException {
        log.debug("REST request to update Accesorios : {}", accesorios);
        if (accesorios.getId() == null) {
            return createAccesorios(accesorios);
        }
        Accesorios result = accesoriosRepository.save(accesorios);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, accesorios.getId().toString()))
            .body(result);
    }

    /**
     * GET  /accesorios : get all the accesorios.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of accesorios in body
     */
    @GetMapping("/accesorios")
    @Timed
    public List<Accesorios> getAllAccesorios() {
        log.debug("REST request to get all Accesorios");
        return accesoriosRepository.findAll();
    }

    /**
     * GET  /accesorios/:id : get the "id" accesorios.
     *
     * @param id the id of the accesorios to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the accesorios, or with status 404 (Not Found)
     */
    @GetMapping("/accesorios/{id}")
    @Timed
    public ResponseEntity<Accesorios> getAccesorios(@PathVariable Long id) {
        log.debug("REST request to get Accesorios : {}", id);
        Accesorios accesorios = accesoriosRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(accesorios));
    }

    /**
     * DELETE  /accesorios/:id : delete the "id" accesorios.
     *
     * @param id the id of the accesorios to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/accesorios/{id}")
    @Timed
    public ResponseEntity<Void> deleteAccesorios(@PathVariable Long id) {
        log.debug("REST request to delete Accesorios : {}", id);
        accesoriosRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
