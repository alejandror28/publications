package com.cartienda.publications.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.cartienda.publications.domain.PublicationStatus;

import com.cartienda.publications.repository.PublicationStatusRepository;
import com.cartienda.publications.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PublicationStatus.
 */
@RestController
@RequestMapping("/api")
public class PublicationStatusResource {

    private final Logger log = LoggerFactory.getLogger(PublicationStatusResource.class);

    private static final String ENTITY_NAME = "publicationStatus";

    private final PublicationStatusRepository publicationStatusRepository;

    public PublicationStatusResource(PublicationStatusRepository publicationStatusRepository) {
        this.publicationStatusRepository = publicationStatusRepository;
    }

    /**
     * POST  /publication-statuses : Create a new publicationStatus.
     *
     * @param publicationStatus the publicationStatus to create
     * @return the ResponseEntity with status 201 (Created) and with body the new publicationStatus, or with status 400 (Bad Request) if the publicationStatus has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/publication-statuses")
    @Timed
    public ResponseEntity<PublicationStatus> createPublicationStatus(@Valid @RequestBody PublicationStatus publicationStatus) throws URISyntaxException {
        log.debug("REST request to save PublicationStatus : {}", publicationStatus);
        if (publicationStatus.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new publicationStatus cannot already have an ID")).body(null);
        }
        PublicationStatus result = publicationStatusRepository.save(publicationStatus);
        return ResponseEntity.created(new URI("/api/publication-statuses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /publication-statuses : Updates an existing publicationStatus.
     *
     * @param publicationStatus the publicationStatus to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated publicationStatus,
     * or with status 400 (Bad Request) if the publicationStatus is not valid,
     * or with status 500 (Internal Server Error) if the publicationStatus couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/publication-statuses")
    @Timed
    public ResponseEntity<PublicationStatus> updatePublicationStatus(@Valid @RequestBody PublicationStatus publicationStatus) throws URISyntaxException {
        log.debug("REST request to update PublicationStatus : {}", publicationStatus);
        if (publicationStatus.getId() == null) {
            return createPublicationStatus(publicationStatus);
        }
        PublicationStatus result = publicationStatusRepository.save(publicationStatus);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, publicationStatus.getId().toString()))
            .body(result);
    }

    /**
     * GET  /publication-statuses : get all the publicationStatuses.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of publicationStatuses in body
     */
    @GetMapping("/publication-statuses")
    @Timed
    public List<PublicationStatus> getAllPublicationStatuses() {
        log.debug("REST request to get all PublicationStatuses");
        return publicationStatusRepository.findAll();
    }

    /**
     * GET  /publication-statuses/:id : get the "id" publicationStatus.
     *
     * @param id the id of the publicationStatus to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the publicationStatus, or with status 404 (Not Found)
     */
    @GetMapping("/publication-statuses/{id}")
    @Timed
    public ResponseEntity<PublicationStatus> getPublicationStatus(@PathVariable Long id) {
        log.debug("REST request to get PublicationStatus : {}", id);
        PublicationStatus publicationStatus = publicationStatusRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(publicationStatus));
    }

    /**
     * DELETE  /publication-statuses/:id : delete the "id" publicationStatus.
     *
     * @param id the id of the publicationStatus to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/publication-statuses/{id}")
    @Timed
    public ResponseEntity<Void> deletePublicationStatus(@PathVariable Long id) {
        log.debug("REST request to delete PublicationStatus : {}", id);
        publicationStatusRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
