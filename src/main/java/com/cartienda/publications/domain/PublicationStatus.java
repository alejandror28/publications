package com.cartienda.publications.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A PublicationStatus.
 */
@Entity
@Table(name = "publication_status")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PublicationStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 255)
    @Column(name = "description", length = 255, nullable = false)
    private String description;

    @OneToMany(mappedBy = "publicationStatus")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Publications> publications = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public PublicationStatus description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Publications> getPublications() {
        return publications;
    }

    public PublicationStatus publications(Set<Publications> publications) {
        this.publications = publications;
        return this;
    }

    public PublicationStatus addPublications(Publications publications) {
        this.publications.add(publications);
        publications.setPublicationStatus(this);
        return this;
    }

    public PublicationStatus removePublications(Publications publications) {
        this.publications.remove(publications);
        publications.setPublicationStatus(null);
        return this;
    }

    public void setPublications(Set<Publications> publications) {
        this.publications = publications;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PublicationStatus publicationStatus = (PublicationStatus) o;
        if (publicationStatus.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), publicationStatus.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PublicationStatus{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
