package com.cartienda.publications.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Accesorios.
 */
@Entity
@Table(name = "accesorios")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Accesorios implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 255)
    @Column(name = "description", length = 255, nullable = false)
    private String description;

    @OneToMany(mappedBy = "accesorios")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<PublicationAccesorios> publicationAccesorios = new HashSet<>();

    @OneToMany(mappedBy = "accesorios")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<TemplatesAccesorios> templatesAccesorios = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public Accesorios description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<PublicationAccesorios> getPublicationAccesorios() {
        return publicationAccesorios;
    }

    public Accesorios publicationAccesorios(Set<PublicationAccesorios> publicationAccesorios) {
        this.publicationAccesorios = publicationAccesorios;
        return this;
    }

    public Accesorios addPublicationAccesorios(PublicationAccesorios publicationAccesorios) {
        this.publicationAccesorios.add(publicationAccesorios);
        publicationAccesorios.setAccesorios(this);
        return this;
    }

    public Accesorios removePublicationAccesorios(PublicationAccesorios publicationAccesorios) {
        this.publicationAccesorios.remove(publicationAccesorios);
        publicationAccesorios.setAccesorios(null);
        return this;
    }

    public void setPublicationAccesorios(Set<PublicationAccesorios> publicationAccesorios) {
        this.publicationAccesorios = publicationAccesorios;
    }

    public Set<TemplatesAccesorios> getTemplatesAccesorios() {
        return templatesAccesorios;
    }

    public Accesorios templatesAccesorios(Set<TemplatesAccesorios> templatesAccesorios) {
        this.templatesAccesorios = templatesAccesorios;
        return this;
    }

    public Accesorios addTemplatesAccesorios(TemplatesAccesorios templatesAccesorios) {
        this.templatesAccesorios.add(templatesAccesorios);
        templatesAccesorios.setAccesorios(this);
        return this;
    }

    public Accesorios removeTemplatesAccesorios(TemplatesAccesorios templatesAccesorios) {
        this.templatesAccesorios.remove(templatesAccesorios);
        templatesAccesorios.setAccesorios(null);
        return this;
    }

    public void setTemplatesAccesorios(Set<TemplatesAccesorios> templatesAccesorios) {
        this.templatesAccesorios = templatesAccesorios;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Accesorios accesorios = (Accesorios) o;
        if (accesorios.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), accesorios.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Accesorios{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
