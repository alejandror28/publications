package com.cartienda.publications.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A TemplatesAccesorios.
 */
@Entity
@Table(name = "templates_accesorios")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TemplatesAccesorios implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @ManyToOne
    private Templates templates;

    @ManyToOne
    private Accesorios accesorios;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Templates getTemplates() {
        return templates;
    }

    public TemplatesAccesorios templates(Templates templates) {
        this.templates = templates;
        return this;
    }

    public void setTemplates(Templates templates) {
        this.templates = templates;
    }

    public Accesorios getAccesorios() {
        return accesorios;
    }

    public TemplatesAccesorios accesorios(Accesorios accesorios) {
        this.accesorios = accesorios;
        return this;
    }

    public void setAccesorios(Accesorios accesorios) {
        this.accesorios = accesorios;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TemplatesAccesorios templatesAccesorios = (TemplatesAccesorios) o;
        if (templatesAccesorios.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), templatesAccesorios.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TemplatesAccesorios{" +
            "id=" + getId() +
            "}";
    }
}
