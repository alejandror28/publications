package com.cartienda.publications.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Publications.
 */
@Entity
@Table(name = "publications")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Publications implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 255)
    @Column(name = "jhi_user", length = 255, nullable = false)
    private String user;

    @Column(name = "creation_date")
    private String creationDate;

    @Column(name = "last_modify_date")
    private String lastModifyDate;

    @Column(name = "vehicle_status")
    private Long vehicleStatus;

    @Column(name = "specifics_car")
    private Long specificsCar;

    @Size(max = 10)
    @Column(name = "car_license", length = 10)
    private String carLicense;

    @Column(name = "plate_publication_types")
    private Long platePublicationTypes;

    @Column(name = "owner_document_types")
    private Long ownerDocumentTypes;

    @Size(max = 25)
    @Column(name = "identification_number", length = 25)
    private String identificationNumber;

    @Column(name = "mileage", precision=10, scale=2)
    private BigDecimal mileage;

    @Column(name = "hours", precision=10, scale=2)
    private BigDecimal hours;

    @Column(name = "price", precision=10, scale=2)
    private BigDecimal price;

    @Column(name = "colors")
    private Long colors;

    @Column(name = "fuel_types")
    private Long fuelTypes;

    @Column(name = "transmission_types")
    private Long transmissionTypes;

    @Column(name = "cylinder")
    private Long cylinder;

    @Column(name = "traction_types")
    private Long tractionTypes;

    @Column(name = "body_work_types")
    private Long bodyWorkTypes;

    @Column(name = "service_types")
    private Long serviceTypes;

    @Column(name = "engine_cylindres")
    private Long engineCylindres;

    @Column(name = "steering_types")
    private Long steeringTypes;

    @Column(name = "transmission_speeds")
    private Long transmissionSpeeds;

    @Column(name = "break_assistance_types")
    private Long breakAssistanceTypes;

    @Column(name = "airbag_types")
    private Long airbagTypes;

    @Column(name = "air_conditionic_types")
    private Long airConditionicTypes;

    @Column(name = "air_conditioner_control_types")
    private Long airConditionerControlTypes;

    @Column(name = "upholstery_types")
    private Long upholsteryTypes;

    @Column(name = "windows_system_types")
    private Long windowsSystemTypes;

    @Column(name = "sunroof_types")
    private Long sunroofTypes;

    @Size(max = 1)
    @Column(name = "financing", length = 1)
    private String financing;

    @Size(max = 1)
    @Column(name = "only_owner", length = 1)
    private String onlyOwner;

    @Size(max = 1)
    @Column(name = "negotiable", length = 1)
    private String negotiable;

    @Size(max = 4)
    @Column(name = "warranty_seller_until", length = 4)
    private String warrantySellerUntil;

    @Size(max = 4)
    @Column(name = "warranty_concessionare_until", length = 4)
    private String warrantyConcessionareUntil;

    @Size(max = 4)
    @Column(name = "soat_currency_up_to", length = 4)
    private String soatCurrencyUpTo;

    @Size(max = 4)
    @Column(name = "tecno_rev_current_up_to", length = 4)
    private String tecnoRevCurrentUpTo;

    @Size(max = 255)
    @Column(name = "text_description", length = 255)
    private String textDescription;

    @OneToMany(mappedBy = "publications")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<PublicationAccesorios> publicationAccesorios = new HashSet<>();

    @ManyToOne
    private PublicationStatus publicationStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public Publications user(String user) {
        this.user = user;
        return this;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public Publications creationDate(String creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getLastModifyDate() {
        return lastModifyDate;
    }

    public Publications lastModifyDate(String lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
        return this;
    }

    public void setLastModifyDate(String lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

    public Long getVehicleStatus() {
        return vehicleStatus;
    }

    public Publications vehicleStatus(Long vehicleStatus) {
        this.vehicleStatus = vehicleStatus;
        return this;
    }

    public void setVehicleStatus(Long vehicleStatus) {
        this.vehicleStatus = vehicleStatus;
    }

    public Long getSpecificsCar() {
        return specificsCar;
    }

    public Publications specificsCar(Long specificsCar) {
        this.specificsCar = specificsCar;
        return this;
    }

    public void setSpecificsCar(Long specificsCar) {
        this.specificsCar = specificsCar;
    }

    public String getCarLicense() {
        return carLicense;
    }

    public Publications carLicense(String carLicense) {
        this.carLicense = carLicense;
        return this;
    }

    public void setCarLicense(String carLicense) {
        this.carLicense = carLicense;
    }

    public Long getPlatePublicationTypes() {
        return platePublicationTypes;
    }

    public Publications platePublicationTypes(Long platePublicationTypes) {
        this.platePublicationTypes = platePublicationTypes;
        return this;
    }

    public void setPlatePublicationTypes(Long platePublicationTypes) {
        this.platePublicationTypes = platePublicationTypes;
    }

    public Long getOwnerDocumentTypes() {
        return ownerDocumentTypes;
    }

    public Publications ownerDocumentTypes(Long ownerDocumentTypes) {
        this.ownerDocumentTypes = ownerDocumentTypes;
        return this;
    }

    public void setOwnerDocumentTypes(Long ownerDocumentTypes) {
        this.ownerDocumentTypes = ownerDocumentTypes;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public Publications identificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
        return this;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public BigDecimal getMileage() {
        return mileage;
    }

    public Publications mileage(BigDecimal mileage) {
        this.mileage = mileage;
        return this;
    }

    public void setMileage(BigDecimal mileage) {
        this.mileage = mileage;
    }

    public BigDecimal getHours() {
        return hours;
    }

    public Publications hours(BigDecimal hours) {
        this.hours = hours;
        return this;
    }

    public void setHours(BigDecimal hours) {
        this.hours = hours;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Publications price(BigDecimal price) {
        this.price = price;
        return this;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getColors() {
        return colors;
    }

    public Publications colors(Long colors) {
        this.colors = colors;
        return this;
    }

    public void setColors(Long colors) {
        this.colors = colors;
    }

    public Long getFuelTypes() {
        return fuelTypes;
    }

    public Publications fuelTypes(Long fuelTypes) {
        this.fuelTypes = fuelTypes;
        return this;
    }

    public void setFuelTypes(Long fuelTypes) {
        this.fuelTypes = fuelTypes;
    }

    public Long getTransmissionTypes() {
        return transmissionTypes;
    }

    public Publications transmissionTypes(Long transmissionTypes) {
        this.transmissionTypes = transmissionTypes;
        return this;
    }

    public void setTransmissionTypes(Long transmissionTypes) {
        this.transmissionTypes = transmissionTypes;
    }

    public Long getCylinder() {
        return cylinder;
    }

    public Publications cylinder(Long cylinder) {
        this.cylinder = cylinder;
        return this;
    }

    public void setCylinder(Long cylinder) {
        this.cylinder = cylinder;
    }

    public Long getTractionTypes() {
        return tractionTypes;
    }

    public Publications tractionTypes(Long tractionTypes) {
        this.tractionTypes = tractionTypes;
        return this;
    }

    public void setTractionTypes(Long tractionTypes) {
        this.tractionTypes = tractionTypes;
    }

    public Long getBodyWorkTypes() {
        return bodyWorkTypes;
    }

    public Publications bodyWorkTypes(Long bodyWorkTypes) {
        this.bodyWorkTypes = bodyWorkTypes;
        return this;
    }

    public void setBodyWorkTypes(Long bodyWorkTypes) {
        this.bodyWorkTypes = bodyWorkTypes;
    }

    public Long getServiceTypes() {
        return serviceTypes;
    }

    public Publications serviceTypes(Long serviceTypes) {
        this.serviceTypes = serviceTypes;
        return this;
    }

    public void setServiceTypes(Long serviceTypes) {
        this.serviceTypes = serviceTypes;
    }

    public Long getEngineCylindres() {
        return engineCylindres;
    }

    public Publications engineCylindres(Long engineCylindres) {
        this.engineCylindres = engineCylindres;
        return this;
    }

    public void setEngineCylindres(Long engineCylindres) {
        this.engineCylindres = engineCylindres;
    }

    public Long getSteeringTypes() {
        return steeringTypes;
    }

    public Publications steeringTypes(Long steeringTypes) {
        this.steeringTypes = steeringTypes;
        return this;
    }

    public void setSteeringTypes(Long steeringTypes) {
        this.steeringTypes = steeringTypes;
    }

    public Long getTransmissionSpeeds() {
        return transmissionSpeeds;
    }

    public Publications transmissionSpeeds(Long transmissionSpeeds) {
        this.transmissionSpeeds = transmissionSpeeds;
        return this;
    }

    public void setTransmissionSpeeds(Long transmissionSpeeds) {
        this.transmissionSpeeds = transmissionSpeeds;
    }

    public Long getBreakAssistanceTypes() {
        return breakAssistanceTypes;
    }

    public Publications breakAssistanceTypes(Long breakAssistanceTypes) {
        this.breakAssistanceTypes = breakAssistanceTypes;
        return this;
    }

    public void setBreakAssistanceTypes(Long breakAssistanceTypes) {
        this.breakAssistanceTypes = breakAssistanceTypes;
    }

    public Long getAirbagTypes() {
        return airbagTypes;
    }

    public Publications airbagTypes(Long airbagTypes) {
        this.airbagTypes = airbagTypes;
        return this;
    }

    public void setAirbagTypes(Long airbagTypes) {
        this.airbagTypes = airbagTypes;
    }

    public Long getAirConditionicTypes() {
        return airConditionicTypes;
    }

    public Publications airConditionicTypes(Long airConditionicTypes) {
        this.airConditionicTypes = airConditionicTypes;
        return this;
    }

    public void setAirConditionicTypes(Long airConditionicTypes) {
        this.airConditionicTypes = airConditionicTypes;
    }

    public Long getAirConditionerControlTypes() {
        return airConditionerControlTypes;
    }

    public Publications airConditionerControlTypes(Long airConditionerControlTypes) {
        this.airConditionerControlTypes = airConditionerControlTypes;
        return this;
    }

    public void setAirConditionerControlTypes(Long airConditionerControlTypes) {
        this.airConditionerControlTypes = airConditionerControlTypes;
    }

    public Long getUpholsteryTypes() {
        return upholsteryTypes;
    }

    public Publications upholsteryTypes(Long upholsteryTypes) {
        this.upholsteryTypes = upholsteryTypes;
        return this;
    }

    public void setUpholsteryTypes(Long upholsteryTypes) {
        this.upholsteryTypes = upholsteryTypes;
    }

    public Long getWindowsSystemTypes() {
        return windowsSystemTypes;
    }

    public Publications windowsSystemTypes(Long windowsSystemTypes) {
        this.windowsSystemTypes = windowsSystemTypes;
        return this;
    }

    public void setWindowsSystemTypes(Long windowsSystemTypes) {
        this.windowsSystemTypes = windowsSystemTypes;
    }

    public Long getSunroofTypes() {
        return sunroofTypes;
    }

    public Publications sunroofTypes(Long sunroofTypes) {
        this.sunroofTypes = sunroofTypes;
        return this;
    }

    public void setSunroofTypes(Long sunroofTypes) {
        this.sunroofTypes = sunroofTypes;
    }

    public String getFinancing() {
        return financing;
    }

    public Publications financing(String financing) {
        this.financing = financing;
        return this;
    }

    public void setFinancing(String financing) {
        this.financing = financing;
    }

    public String getOnlyOwner() {
        return onlyOwner;
    }

    public Publications onlyOwner(String onlyOwner) {
        this.onlyOwner = onlyOwner;
        return this;
    }

    public void setOnlyOwner(String onlyOwner) {
        this.onlyOwner = onlyOwner;
    }

    public String getNegotiable() {
        return negotiable;
    }

    public Publications negotiable(String negotiable) {
        this.negotiable = negotiable;
        return this;
    }

    public void setNegotiable(String negotiable) {
        this.negotiable = negotiable;
    }

    public String getWarrantySellerUntil() {
        return warrantySellerUntil;
    }

    public Publications warrantySellerUntil(String warrantySellerUntil) {
        this.warrantySellerUntil = warrantySellerUntil;
        return this;
    }

    public void setWarrantySellerUntil(String warrantySellerUntil) {
        this.warrantySellerUntil = warrantySellerUntil;
    }

    public String getWarrantyConcessionareUntil() {
        return warrantyConcessionareUntil;
    }

    public Publications warrantyConcessionareUntil(String warrantyConcessionareUntil) {
        this.warrantyConcessionareUntil = warrantyConcessionareUntil;
        return this;
    }

    public void setWarrantyConcessionareUntil(String warrantyConcessionareUntil) {
        this.warrantyConcessionareUntil = warrantyConcessionareUntil;
    }

    public String getSoatCurrencyUpTo() {
        return soatCurrencyUpTo;
    }

    public Publications soatCurrencyUpTo(String soatCurrencyUpTo) {
        this.soatCurrencyUpTo = soatCurrencyUpTo;
        return this;
    }

    public void setSoatCurrencyUpTo(String soatCurrencyUpTo) {
        this.soatCurrencyUpTo = soatCurrencyUpTo;
    }

    public String getTecnoRevCurrentUpTo() {
        return tecnoRevCurrentUpTo;
    }

    public Publications tecnoRevCurrentUpTo(String tecnoRevCurrentUpTo) {
        this.tecnoRevCurrentUpTo = tecnoRevCurrentUpTo;
        return this;
    }

    public void setTecnoRevCurrentUpTo(String tecnoRevCurrentUpTo) {
        this.tecnoRevCurrentUpTo = tecnoRevCurrentUpTo;
    }

    public String getTextDescription() {
        return textDescription;
    }

    public Publications textDescription(String textDescription) {
        this.textDescription = textDescription;
        return this;
    }

    public void setTextDescription(String textDescription) {
        this.textDescription = textDescription;
    }

    public Set<PublicationAccesorios> getPublicationAccesorios() {
        return publicationAccesorios;
    }

    public Publications publicationAccesorios(Set<PublicationAccesorios> publicationAccesorios) {
        this.publicationAccesorios = publicationAccesorios;
        return this;
    }

    public Publications addPublicationAccesorios(PublicationAccesorios publicationAccesorios) {
        this.publicationAccesorios.add(publicationAccesorios);
        publicationAccesorios.setPublications(this);
        return this;
    }

    public Publications removePublicationAccesorios(PublicationAccesorios publicationAccesorios) {
        this.publicationAccesorios.remove(publicationAccesorios);
        publicationAccesorios.setPublications(null);
        return this;
    }

    public void setPublicationAccesorios(Set<PublicationAccesorios> publicationAccesorios) {
        this.publicationAccesorios = publicationAccesorios;
    }

    public PublicationStatus getPublicationStatus() {
        return publicationStatus;
    }

    public Publications publicationStatus(PublicationStatus publicationStatus) {
        this.publicationStatus = publicationStatus;
        return this;
    }

    public void setPublicationStatus(PublicationStatus publicationStatus) {
        this.publicationStatus = publicationStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Publications publications = (Publications) o;
        if (publications.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), publications.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Publications{" +
            "id=" + getId() +
            ", user='" + getUser() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", lastModifyDate='" + getLastModifyDate() + "'" +
            ", vehicleStatus='" + getVehicleStatus() + "'" +
            ", specificsCar='" + getSpecificsCar() + "'" +
            ", carLicense='" + getCarLicense() + "'" +
            ", platePublicationTypes='" + getPlatePublicationTypes() + "'" +
            ", ownerDocumentTypes='" + getOwnerDocumentTypes() + "'" +
            ", identificationNumber='" + getIdentificationNumber() + "'" +
            ", mileage='" + getMileage() + "'" +
            ", hours='" + getHours() + "'" +
            ", price='" + getPrice() + "'" +
            ", colors='" + getColors() + "'" +
            ", fuelTypes='" + getFuelTypes() + "'" +
            ", transmissionTypes='" + getTransmissionTypes() + "'" +
            ", cylinder='" + getCylinder() + "'" +
            ", tractionTypes='" + getTractionTypes() + "'" +
            ", bodyWorkTypes='" + getBodyWorkTypes() + "'" +
            ", serviceTypes='" + getServiceTypes() + "'" +
            ", engineCylindres='" + getEngineCylindres() + "'" +
            ", steeringTypes='" + getSteeringTypes() + "'" +
            ", transmissionSpeeds='" + getTransmissionSpeeds() + "'" +
            ", breakAssistanceTypes='" + getBreakAssistanceTypes() + "'" +
            ", airbagTypes='" + getAirbagTypes() + "'" +
            ", airConditionicTypes='" + getAirConditionicTypes() + "'" +
            ", airConditionerControlTypes='" + getAirConditionerControlTypes() + "'" +
            ", upholsteryTypes='" + getUpholsteryTypes() + "'" +
            ", windowsSystemTypes='" + getWindowsSystemTypes() + "'" +
            ", sunroofTypes='" + getSunroofTypes() + "'" +
            ", financing='" + getFinancing() + "'" +
            ", onlyOwner='" + getOnlyOwner() + "'" +
            ", negotiable='" + getNegotiable() + "'" +
            ", warrantySellerUntil='" + getWarrantySellerUntil() + "'" +
            ", warrantyConcessionareUntil='" + getWarrantyConcessionareUntil() + "'" +
            ", soatCurrencyUpTo='" + getSoatCurrencyUpTo() + "'" +
            ", tecnoRevCurrentUpTo='" + getTecnoRevCurrentUpTo() + "'" +
            ", textDescription='" + getTextDescription() + "'" +
            "}";
    }
}
