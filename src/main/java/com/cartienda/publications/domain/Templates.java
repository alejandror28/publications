package com.cartienda.publications.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Templates.
 */
@Entity
@Table(name = "templates")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Templates implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "specifics_car")
    private Long specificsCar;

    @Column(name = "colors")
    private Long colors;

    @Column(name = "fuel_types")
    private Long fuelTypes;

    @Column(name = "transmission_types")
    private Long transmissionTypes;

    @Column(name = "cylinder")
    private Long cylinder;

    @Column(name = "traction_types")
    private Long tractionTypes;

    @Column(name = "body_work_types")
    private Long bodyWorkTypes;

    @Column(name = "service_types")
    private Long serviceTypes;

    @Column(name = "engine_cylindres")
    private Long engineCylindres;

    @Column(name = "steering_types")
    private Long steeringTypes;

    @Column(name = "transmission_speeds")
    private Long transmissionSpeeds;

    @Column(name = "break_assistance_types")
    private Long breakAssistanceTypes;

    @Column(name = "airbag_types")
    private Long airbagTypes;

    @Column(name = "air_conditionic_types")
    private Long airConditionicTypes;

    @Column(name = "air_conditioner_control_types")
    private Long airConditionerControlTypes;

    @Column(name = "upholstery_types")
    private Long upholsteryTypes;

    @Column(name = "windows_system_types")
    private Long windowsSystemTypes;

    @Column(name = "sunroof_types")
    private Long sunroofTypes;

    @OneToMany(mappedBy = "templates")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<TemplatesAccesorios> templatesAccesorios = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSpecificsCar() {
        return specificsCar;
    }

    public Templates specificsCar(Long specificsCar) {
        this.specificsCar = specificsCar;
        return this;
    }

    public void setSpecificsCar(Long specificsCar) {
        this.specificsCar = specificsCar;
    }

    public Long getColors() {
        return colors;
    }

    public Templates colors(Long colors) {
        this.colors = colors;
        return this;
    }

    public void setColors(Long colors) {
        this.colors = colors;
    }

    public Long getFuelTypes() {
        return fuelTypes;
    }

    public Templates fuelTypes(Long fuelTypes) {
        this.fuelTypes = fuelTypes;
        return this;
    }

    public void setFuelTypes(Long fuelTypes) {
        this.fuelTypes = fuelTypes;
    }

    public Long getTransmissionTypes() {
        return transmissionTypes;
    }

    public Templates transmissionTypes(Long transmissionTypes) {
        this.transmissionTypes = transmissionTypes;
        return this;
    }

    public void setTransmissionTypes(Long transmissionTypes) {
        this.transmissionTypes = transmissionTypes;
    }

    public Long getCylinder() {
        return cylinder;
    }

    public Templates cylinder(Long cylinder) {
        this.cylinder = cylinder;
        return this;
    }

    public void setCylinder(Long cylinder) {
        this.cylinder = cylinder;
    }

    public Long getTractionTypes() {
        return tractionTypes;
    }

    public Templates tractionTypes(Long tractionTypes) {
        this.tractionTypes = tractionTypes;
        return this;
    }

    public void setTractionTypes(Long tractionTypes) {
        this.tractionTypes = tractionTypes;
    }

    public Long getBodyWorkTypes() {
        return bodyWorkTypes;
    }

    public Templates bodyWorkTypes(Long bodyWorkTypes) {
        this.bodyWorkTypes = bodyWorkTypes;
        return this;
    }

    public void setBodyWorkTypes(Long bodyWorkTypes) {
        this.bodyWorkTypes = bodyWorkTypes;
    }

    public Long getServiceTypes() {
        return serviceTypes;
    }

    public Templates serviceTypes(Long serviceTypes) {
        this.serviceTypes = serviceTypes;
        return this;
    }

    public void setServiceTypes(Long serviceTypes) {
        this.serviceTypes = serviceTypes;
    }

    public Long getEngineCylindres() {
        return engineCylindres;
    }

    public Templates engineCylindres(Long engineCylindres) {
        this.engineCylindres = engineCylindres;
        return this;
    }

    public void setEngineCylindres(Long engineCylindres) {
        this.engineCylindres = engineCylindres;
    }

    public Long getSteeringTypes() {
        return steeringTypes;
    }

    public Templates steeringTypes(Long steeringTypes) {
        this.steeringTypes = steeringTypes;
        return this;
    }

    public void setSteeringTypes(Long steeringTypes) {
        this.steeringTypes = steeringTypes;
    }

    public Long getTransmissionSpeeds() {
        return transmissionSpeeds;
    }

    public Templates transmissionSpeeds(Long transmissionSpeeds) {
        this.transmissionSpeeds = transmissionSpeeds;
        return this;
    }

    public void setTransmissionSpeeds(Long transmissionSpeeds) {
        this.transmissionSpeeds = transmissionSpeeds;
    }

    public Long getBreakAssistanceTypes() {
        return breakAssistanceTypes;
    }

    public Templates breakAssistanceTypes(Long breakAssistanceTypes) {
        this.breakAssistanceTypes = breakAssistanceTypes;
        return this;
    }

    public void setBreakAssistanceTypes(Long breakAssistanceTypes) {
        this.breakAssistanceTypes = breakAssistanceTypes;
    }

    public Long getAirbagTypes() {
        return airbagTypes;
    }

    public Templates airbagTypes(Long airbagTypes) {
        this.airbagTypes = airbagTypes;
        return this;
    }

    public void setAirbagTypes(Long airbagTypes) {
        this.airbagTypes = airbagTypes;
    }

    public Long getAirConditionicTypes() {
        return airConditionicTypes;
    }

    public Templates airConditionicTypes(Long airConditionicTypes) {
        this.airConditionicTypes = airConditionicTypes;
        return this;
    }

    public void setAirConditionicTypes(Long airConditionicTypes) {
        this.airConditionicTypes = airConditionicTypes;
    }

    public Long getAirConditionerControlTypes() {
        return airConditionerControlTypes;
    }

    public Templates airConditionerControlTypes(Long airConditionerControlTypes) {
        this.airConditionerControlTypes = airConditionerControlTypes;
        return this;
    }

    public void setAirConditionerControlTypes(Long airConditionerControlTypes) {
        this.airConditionerControlTypes = airConditionerControlTypes;
    }

    public Long getUpholsteryTypes() {
        return upholsteryTypes;
    }

    public Templates upholsteryTypes(Long upholsteryTypes) {
        this.upholsteryTypes = upholsteryTypes;
        return this;
    }

    public void setUpholsteryTypes(Long upholsteryTypes) {
        this.upholsteryTypes = upholsteryTypes;
    }

    public Long getWindowsSystemTypes() {
        return windowsSystemTypes;
    }

    public Templates windowsSystemTypes(Long windowsSystemTypes) {
        this.windowsSystemTypes = windowsSystemTypes;
        return this;
    }

    public void setWindowsSystemTypes(Long windowsSystemTypes) {
        this.windowsSystemTypes = windowsSystemTypes;
    }

    public Long getSunroofTypes() {
        return sunroofTypes;
    }

    public Templates sunroofTypes(Long sunroofTypes) {
        this.sunroofTypes = sunroofTypes;
        return this;
    }

    public void setSunroofTypes(Long sunroofTypes) {
        this.sunroofTypes = sunroofTypes;
    }

    public Set<TemplatesAccesorios> getTemplatesAccesorios() {
        return templatesAccesorios;
    }

    public Templates templatesAccesorios(Set<TemplatesAccesorios> templatesAccesorios) {
        this.templatesAccesorios = templatesAccesorios;
        return this;
    }

    public Templates addTemplatesAccesorios(TemplatesAccesorios templatesAccesorios) {
        this.templatesAccesorios.add(templatesAccesorios);
        templatesAccesorios.setTemplates(this);
        return this;
    }

    public Templates removeTemplatesAccesorios(TemplatesAccesorios templatesAccesorios) {
        this.templatesAccesorios.remove(templatesAccesorios);
        templatesAccesorios.setTemplates(null);
        return this;
    }

    public void setTemplatesAccesorios(Set<TemplatesAccesorios> templatesAccesorios) {
        this.templatesAccesorios = templatesAccesorios;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Templates templates = (Templates) o;
        if (templates.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), templates.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Templates{" +
            "id=" + getId() +
            ", specificsCar='" + getSpecificsCar() + "'" +
            ", colors='" + getColors() + "'" +
            ", fuelTypes='" + getFuelTypes() + "'" +
            ", transmissionTypes='" + getTransmissionTypes() + "'" +
            ", cylinder='" + getCylinder() + "'" +
            ", tractionTypes='" + getTractionTypes() + "'" +
            ", bodyWorkTypes='" + getBodyWorkTypes() + "'" +
            ", serviceTypes='" + getServiceTypes() + "'" +
            ", engineCylindres='" + getEngineCylindres() + "'" +
            ", steeringTypes='" + getSteeringTypes() + "'" +
            ", transmissionSpeeds='" + getTransmissionSpeeds() + "'" +
            ", breakAssistanceTypes='" + getBreakAssistanceTypes() + "'" +
            ", airbagTypes='" + getAirbagTypes() + "'" +
            ", airConditionicTypes='" + getAirConditionicTypes() + "'" +
            ", airConditionerControlTypes='" + getAirConditionerControlTypes() + "'" +
            ", upholsteryTypes='" + getUpholsteryTypes() + "'" +
            ", windowsSystemTypes='" + getWindowsSystemTypes() + "'" +
            ", sunroofTypes='" + getSunroofTypes() + "'" +
            "}";
    }
}
