package com.cartienda.publications.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A PublicationAccesorios.
 */
@Entity
@Table(name = "publication_accesorios")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PublicationAccesorios implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @ManyToOne
    private Publications publications;

    @ManyToOne
    private Accesorios accesorios;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Publications getPublications() {
        return publications;
    }

    public PublicationAccesorios publications(Publications publications) {
        this.publications = publications;
        return this;
    }

    public void setPublications(Publications publications) {
        this.publications = publications;
    }

    public Accesorios getAccesorios() {
        return accesorios;
    }

    public PublicationAccesorios accesorios(Accesorios accesorios) {
        this.accesorios = accesorios;
        return this;
    }

    public void setAccesorios(Accesorios accesorios) {
        this.accesorios = accesorios;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PublicationAccesorios publicationAccesorios = (PublicationAccesorios) o;
        if (publicationAccesorios.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), publicationAccesorios.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PublicationAccesorios{" +
            "id=" + getId() +
            "}";
    }
}
