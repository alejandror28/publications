package com.cartienda.publications.web.rest;

import com.cartienda.publications.PublicationsApp;

import com.cartienda.publications.domain.Publications;
import com.cartienda.publications.repository.PublicationsRepository;
import com.cartienda.publications.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PublicationsResource REST controller.
 *
 * @see PublicationsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PublicationsApp.class)
public class PublicationsResourceIntTest {

    private static final String DEFAULT_USER = "AAAAAAAAAA";
    private static final String UPDATED_USER = "BBBBBBBBBB";

    private static final String DEFAULT_CREATION_DATE = "AAAAAAAAAA";
    private static final String UPDATED_CREATION_DATE = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_MODIFY_DATE = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFY_DATE = "BBBBBBBBBB";

    private static final Long DEFAULT_VEHICLE_STATUS = 1L;
    private static final Long UPDATED_VEHICLE_STATUS = 2L;

    private static final Long DEFAULT_SPECIFICS_CAR = 1L;
    private static final Long UPDATED_SPECIFICS_CAR = 2L;

    private static final String DEFAULT_CAR_LICENSE = "AAAAAAAAAA";
    private static final String UPDATED_CAR_LICENSE = "BBBBBBBBBB";

    private static final Long DEFAULT_PLATE_PUBLICATION_TYPES = 1L;
    private static final Long UPDATED_PLATE_PUBLICATION_TYPES = 2L;

    private static final Long DEFAULT_OWNER_DOCUMENT_TYPES = 1L;
    private static final Long UPDATED_OWNER_DOCUMENT_TYPES = 2L;

    private static final String DEFAULT_IDENTIFICATION_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_IDENTIFICATION_NUMBER = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_MILEAGE = new BigDecimal(1);
    private static final BigDecimal UPDATED_MILEAGE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_HOURS = new BigDecimal(1);
    private static final BigDecimal UPDATED_HOURS = new BigDecimal(2);

    private static final BigDecimal DEFAULT_PRICE = new BigDecimal(1);
    private static final BigDecimal UPDATED_PRICE = new BigDecimal(2);

    private static final Long DEFAULT_COLORS = 1L;
    private static final Long UPDATED_COLORS = 2L;

    private static final Long DEFAULT_FUEL_TYPES = 1L;
    private static final Long UPDATED_FUEL_TYPES = 2L;

    private static final Long DEFAULT_TRANSMISSION_TYPES = 1L;
    private static final Long UPDATED_TRANSMISSION_TYPES = 2L;

    private static final Long DEFAULT_CYLINDER = 1L;
    private static final Long UPDATED_CYLINDER = 2L;

    private static final Long DEFAULT_TRACTION_TYPES = 1L;
    private static final Long UPDATED_TRACTION_TYPES = 2L;

    private static final Long DEFAULT_BODY_WORK_TYPES = 1L;
    private static final Long UPDATED_BODY_WORK_TYPES = 2L;

    private static final Long DEFAULT_SERVICE_TYPES = 1L;
    private static final Long UPDATED_SERVICE_TYPES = 2L;

    private static final Long DEFAULT_ENGINE_CYLINDRES = 1L;
    private static final Long UPDATED_ENGINE_CYLINDRES = 2L;

    private static final Long DEFAULT_STEERING_TYPES = 1L;
    private static final Long UPDATED_STEERING_TYPES = 2L;

    private static final Long DEFAULT_TRANSMISSION_SPEEDS = 1L;
    private static final Long UPDATED_TRANSMISSION_SPEEDS = 2L;

    private static final Long DEFAULT_BREAK_ASSISTANCE_TYPES = 1L;
    private static final Long UPDATED_BREAK_ASSISTANCE_TYPES = 2L;

    private static final Long DEFAULT_AIRBAG_TYPES = 1L;
    private static final Long UPDATED_AIRBAG_TYPES = 2L;

    private static final Long DEFAULT_AIR_CONDITIONIC_TYPES = 1L;
    private static final Long UPDATED_AIR_CONDITIONIC_TYPES = 2L;

    private static final Long DEFAULT_AIR_CONDITIONER_CONTROL_TYPES = 1L;
    private static final Long UPDATED_AIR_CONDITIONER_CONTROL_TYPES = 2L;

    private static final Long DEFAULT_UPHOLSTERY_TYPES = 1L;
    private static final Long UPDATED_UPHOLSTERY_TYPES = 2L;

    private static final Long DEFAULT_WINDOWS_SYSTEM_TYPES = 1L;
    private static final Long UPDATED_WINDOWS_SYSTEM_TYPES = 2L;

    private static final Long DEFAULT_SUNROOF_TYPES = 1L;
    private static final Long UPDATED_SUNROOF_TYPES = 2L;

    private static final String DEFAULT_FINANCING = "A";
    private static final String UPDATED_FINANCING = "B";

    private static final String DEFAULT_ONLY_OWNER = "A";
    private static final String UPDATED_ONLY_OWNER = "B";

    private static final String DEFAULT_NEGOTIABLE = "A";
    private static final String UPDATED_NEGOTIABLE = "B";

    private static final String DEFAULT_WARRANTY_SELLER_UNTIL = "AAAA";
    private static final String UPDATED_WARRANTY_SELLER_UNTIL = "BBBB";

    private static final String DEFAULT_WARRANTY_CONCESSIONARE_UNTIL = "AAAA";
    private static final String UPDATED_WARRANTY_CONCESSIONARE_UNTIL = "BBBB";

    private static final String DEFAULT_SOAT_CURRENCY_UP_TO = "AAAA";
    private static final String UPDATED_SOAT_CURRENCY_UP_TO = "BBBB";

    private static final String DEFAULT_TECNO_REV_CURRENT_UP_TO = "AAAA";
    private static final String UPDATED_TECNO_REV_CURRENT_UP_TO = "BBBB";

    private static final String DEFAULT_TEXT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_TEXT_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private PublicationsRepository publicationsRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPublicationsMockMvc;

    private Publications publications;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PublicationsResource publicationsResource = new PublicationsResource(publicationsRepository);
        this.restPublicationsMockMvc = MockMvcBuilders.standaloneSetup(publicationsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Publications createEntity(EntityManager em) {
        Publications publications = new Publications()
            .user(DEFAULT_USER)
            .creationDate(DEFAULT_CREATION_DATE)
            .lastModifyDate(DEFAULT_LAST_MODIFY_DATE)
            .vehicleStatus(DEFAULT_VEHICLE_STATUS)
            .specificsCar(DEFAULT_SPECIFICS_CAR)
            .carLicense(DEFAULT_CAR_LICENSE)
            .platePublicationTypes(DEFAULT_PLATE_PUBLICATION_TYPES)
            .ownerDocumentTypes(DEFAULT_OWNER_DOCUMENT_TYPES)
            .identificationNumber(DEFAULT_IDENTIFICATION_NUMBER)
            .mileage(DEFAULT_MILEAGE)
            .hours(DEFAULT_HOURS)
            .price(DEFAULT_PRICE)
            .colors(DEFAULT_COLORS)
            .fuelTypes(DEFAULT_FUEL_TYPES)
            .transmissionTypes(DEFAULT_TRANSMISSION_TYPES)
            .cylinder(DEFAULT_CYLINDER)
            .tractionTypes(DEFAULT_TRACTION_TYPES)
            .bodyWorkTypes(DEFAULT_BODY_WORK_TYPES)
            .serviceTypes(DEFAULT_SERVICE_TYPES)
            .engineCylindres(DEFAULT_ENGINE_CYLINDRES)
            .steeringTypes(DEFAULT_STEERING_TYPES)
            .transmissionSpeeds(DEFAULT_TRANSMISSION_SPEEDS)
            .breakAssistanceTypes(DEFAULT_BREAK_ASSISTANCE_TYPES)
            .airbagTypes(DEFAULT_AIRBAG_TYPES)
            .airConditionicTypes(DEFAULT_AIR_CONDITIONIC_TYPES)
            .airConditionerControlTypes(DEFAULT_AIR_CONDITIONER_CONTROL_TYPES)
            .upholsteryTypes(DEFAULT_UPHOLSTERY_TYPES)
            .windowsSystemTypes(DEFAULT_WINDOWS_SYSTEM_TYPES)
            .sunroofTypes(DEFAULT_SUNROOF_TYPES)
            .financing(DEFAULT_FINANCING)
            .onlyOwner(DEFAULT_ONLY_OWNER)
            .negotiable(DEFAULT_NEGOTIABLE)
            .warrantySellerUntil(DEFAULT_WARRANTY_SELLER_UNTIL)
            .warrantyConcessionareUntil(DEFAULT_WARRANTY_CONCESSIONARE_UNTIL)
            .soatCurrencyUpTo(DEFAULT_SOAT_CURRENCY_UP_TO)
            .tecnoRevCurrentUpTo(DEFAULT_TECNO_REV_CURRENT_UP_TO)
            .textDescription(DEFAULT_TEXT_DESCRIPTION);
        return publications;
    }

    @Before
    public void initTest() {
        publications = createEntity(em);
    }

    @Test
    @Transactional
    public void createPublications() throws Exception {
        int databaseSizeBeforeCreate = publicationsRepository.findAll().size();

        // Create the Publications
        restPublicationsMockMvc.perform(post("/api/publications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(publications)))
            .andExpect(status().isCreated());

        // Validate the Publications in the database
        List<Publications> publicationsList = publicationsRepository.findAll();
        assertThat(publicationsList).hasSize(databaseSizeBeforeCreate + 1);
        Publications testPublications = publicationsList.get(publicationsList.size() - 1);
        assertThat(testPublications.getUser()).isEqualTo(DEFAULT_USER);
        assertThat(testPublications.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testPublications.getLastModifyDate()).isEqualTo(DEFAULT_LAST_MODIFY_DATE);
        assertThat(testPublications.getVehicleStatus()).isEqualTo(DEFAULT_VEHICLE_STATUS);
        assertThat(testPublications.getSpecificsCar()).isEqualTo(DEFAULT_SPECIFICS_CAR);
        assertThat(testPublications.getCarLicense()).isEqualTo(DEFAULT_CAR_LICENSE);
        assertThat(testPublications.getPlatePublicationTypes()).isEqualTo(DEFAULT_PLATE_PUBLICATION_TYPES);
        assertThat(testPublications.getOwnerDocumentTypes()).isEqualTo(DEFAULT_OWNER_DOCUMENT_TYPES);
        assertThat(testPublications.getIdentificationNumber()).isEqualTo(DEFAULT_IDENTIFICATION_NUMBER);
        assertThat(testPublications.getMileage()).isEqualTo(DEFAULT_MILEAGE);
        assertThat(testPublications.getHours()).isEqualTo(DEFAULT_HOURS);
        assertThat(testPublications.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testPublications.getColors()).isEqualTo(DEFAULT_COLORS);
        assertThat(testPublications.getFuelTypes()).isEqualTo(DEFAULT_FUEL_TYPES);
        assertThat(testPublications.getTransmissionTypes()).isEqualTo(DEFAULT_TRANSMISSION_TYPES);
        assertThat(testPublications.getCylinder()).isEqualTo(DEFAULT_CYLINDER);
        assertThat(testPublications.getTractionTypes()).isEqualTo(DEFAULT_TRACTION_TYPES);
        assertThat(testPublications.getBodyWorkTypes()).isEqualTo(DEFAULT_BODY_WORK_TYPES);
        assertThat(testPublications.getServiceTypes()).isEqualTo(DEFAULT_SERVICE_TYPES);
        assertThat(testPublications.getEngineCylindres()).isEqualTo(DEFAULT_ENGINE_CYLINDRES);
        assertThat(testPublications.getSteeringTypes()).isEqualTo(DEFAULT_STEERING_TYPES);
        assertThat(testPublications.getTransmissionSpeeds()).isEqualTo(DEFAULT_TRANSMISSION_SPEEDS);
        assertThat(testPublications.getBreakAssistanceTypes()).isEqualTo(DEFAULT_BREAK_ASSISTANCE_TYPES);
        assertThat(testPublications.getAirbagTypes()).isEqualTo(DEFAULT_AIRBAG_TYPES);
        assertThat(testPublications.getAirConditionicTypes()).isEqualTo(DEFAULT_AIR_CONDITIONIC_TYPES);
        assertThat(testPublications.getAirConditionerControlTypes()).isEqualTo(DEFAULT_AIR_CONDITIONER_CONTROL_TYPES);
        assertThat(testPublications.getUpholsteryTypes()).isEqualTo(DEFAULT_UPHOLSTERY_TYPES);
        assertThat(testPublications.getWindowsSystemTypes()).isEqualTo(DEFAULT_WINDOWS_SYSTEM_TYPES);
        assertThat(testPublications.getSunroofTypes()).isEqualTo(DEFAULT_SUNROOF_TYPES);
        assertThat(testPublications.getFinancing()).isEqualTo(DEFAULT_FINANCING);
        assertThat(testPublications.getOnlyOwner()).isEqualTo(DEFAULT_ONLY_OWNER);
        assertThat(testPublications.getNegotiable()).isEqualTo(DEFAULT_NEGOTIABLE);
        assertThat(testPublications.getWarrantySellerUntil()).isEqualTo(DEFAULT_WARRANTY_SELLER_UNTIL);
        assertThat(testPublications.getWarrantyConcessionareUntil()).isEqualTo(DEFAULT_WARRANTY_CONCESSIONARE_UNTIL);
        assertThat(testPublications.getSoatCurrencyUpTo()).isEqualTo(DEFAULT_SOAT_CURRENCY_UP_TO);
        assertThat(testPublications.getTecnoRevCurrentUpTo()).isEqualTo(DEFAULT_TECNO_REV_CURRENT_UP_TO);
        assertThat(testPublications.getTextDescription()).isEqualTo(DEFAULT_TEXT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createPublicationsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = publicationsRepository.findAll().size();

        // Create the Publications with an existing ID
        publications.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPublicationsMockMvc.perform(post("/api/publications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(publications)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Publications> publicationsList = publicationsRepository.findAll();
        assertThat(publicationsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkUserIsRequired() throws Exception {
        int databaseSizeBeforeTest = publicationsRepository.findAll().size();
        // set the field null
        publications.setUser(null);

        // Create the Publications, which fails.

        restPublicationsMockMvc.perform(post("/api/publications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(publications)))
            .andExpect(status().isBadRequest());

        List<Publications> publicationsList = publicationsRepository.findAll();
        assertThat(publicationsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPublications() throws Exception {
        // Initialize the database
        publicationsRepository.saveAndFlush(publications);

        // Get all the publicationsList
        restPublicationsMockMvc.perform(get("/api/publications?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(publications.getId().intValue())))
            .andExpect(jsonPath("$.[*].user").value(hasItem(DEFAULT_USER.toString())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifyDate").value(hasItem(DEFAULT_LAST_MODIFY_DATE.toString())))
            .andExpect(jsonPath("$.[*].vehicleStatus").value(hasItem(DEFAULT_VEHICLE_STATUS.intValue())))
            .andExpect(jsonPath("$.[*].specificsCar").value(hasItem(DEFAULT_SPECIFICS_CAR.intValue())))
            .andExpect(jsonPath("$.[*].carLicense").value(hasItem(DEFAULT_CAR_LICENSE.toString())))
            .andExpect(jsonPath("$.[*].platePublicationTypes").value(hasItem(DEFAULT_PLATE_PUBLICATION_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].ownerDocumentTypes").value(hasItem(DEFAULT_OWNER_DOCUMENT_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].identificationNumber").value(hasItem(DEFAULT_IDENTIFICATION_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].mileage").value(hasItem(DEFAULT_MILEAGE.intValue())))
            .andExpect(jsonPath("$.[*].hours").value(hasItem(DEFAULT_HOURS.intValue())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].colors").value(hasItem(DEFAULT_COLORS.intValue())))
            .andExpect(jsonPath("$.[*].fuelTypes").value(hasItem(DEFAULT_FUEL_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].transmissionTypes").value(hasItem(DEFAULT_TRANSMISSION_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].cylinder").value(hasItem(DEFAULT_CYLINDER.intValue())))
            .andExpect(jsonPath("$.[*].tractionTypes").value(hasItem(DEFAULT_TRACTION_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].bodyWorkTypes").value(hasItem(DEFAULT_BODY_WORK_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].serviceTypes").value(hasItem(DEFAULT_SERVICE_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].engineCylindres").value(hasItem(DEFAULT_ENGINE_CYLINDRES.intValue())))
            .andExpect(jsonPath("$.[*].steeringTypes").value(hasItem(DEFAULT_STEERING_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].transmissionSpeeds").value(hasItem(DEFAULT_TRANSMISSION_SPEEDS.intValue())))
            .andExpect(jsonPath("$.[*].breakAssistanceTypes").value(hasItem(DEFAULT_BREAK_ASSISTANCE_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].airbagTypes").value(hasItem(DEFAULT_AIRBAG_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].airConditionicTypes").value(hasItem(DEFAULT_AIR_CONDITIONIC_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].airConditionerControlTypes").value(hasItem(DEFAULT_AIR_CONDITIONER_CONTROL_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].upholsteryTypes").value(hasItem(DEFAULT_UPHOLSTERY_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].windowsSystemTypes").value(hasItem(DEFAULT_WINDOWS_SYSTEM_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].sunroofTypes").value(hasItem(DEFAULT_SUNROOF_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].financing").value(hasItem(DEFAULT_FINANCING.toString())))
            .andExpect(jsonPath("$.[*].onlyOwner").value(hasItem(DEFAULT_ONLY_OWNER.toString())))
            .andExpect(jsonPath("$.[*].negotiable").value(hasItem(DEFAULT_NEGOTIABLE.toString())))
            .andExpect(jsonPath("$.[*].warrantySellerUntil").value(hasItem(DEFAULT_WARRANTY_SELLER_UNTIL.toString())))
            .andExpect(jsonPath("$.[*].warrantyConcessionareUntil").value(hasItem(DEFAULT_WARRANTY_CONCESSIONARE_UNTIL.toString())))
            .andExpect(jsonPath("$.[*].soatCurrencyUpTo").value(hasItem(DEFAULT_SOAT_CURRENCY_UP_TO.toString())))
            .andExpect(jsonPath("$.[*].tecnoRevCurrentUpTo").value(hasItem(DEFAULT_TECNO_REV_CURRENT_UP_TO.toString())))
            .andExpect(jsonPath("$.[*].textDescription").value(hasItem(DEFAULT_TEXT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getPublications() throws Exception {
        // Initialize the database
        publicationsRepository.saveAndFlush(publications);

        // Get the publications
        restPublicationsMockMvc.perform(get("/api/publications/{id}", publications.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(publications.getId().intValue()))
            .andExpect(jsonPath("$.user").value(DEFAULT_USER.toString()))
            .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.lastModifyDate").value(DEFAULT_LAST_MODIFY_DATE.toString()))
            .andExpect(jsonPath("$.vehicleStatus").value(DEFAULT_VEHICLE_STATUS.intValue()))
            .andExpect(jsonPath("$.specificsCar").value(DEFAULT_SPECIFICS_CAR.intValue()))
            .andExpect(jsonPath("$.carLicense").value(DEFAULT_CAR_LICENSE.toString()))
            .andExpect(jsonPath("$.platePublicationTypes").value(DEFAULT_PLATE_PUBLICATION_TYPES.intValue()))
            .andExpect(jsonPath("$.ownerDocumentTypes").value(DEFAULT_OWNER_DOCUMENT_TYPES.intValue()))
            .andExpect(jsonPath("$.identificationNumber").value(DEFAULT_IDENTIFICATION_NUMBER.toString()))
            .andExpect(jsonPath("$.mileage").value(DEFAULT_MILEAGE.intValue()))
            .andExpect(jsonPath("$.hours").value(DEFAULT_HOURS.intValue()))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.intValue()))
            .andExpect(jsonPath("$.colors").value(DEFAULT_COLORS.intValue()))
            .andExpect(jsonPath("$.fuelTypes").value(DEFAULT_FUEL_TYPES.intValue()))
            .andExpect(jsonPath("$.transmissionTypes").value(DEFAULT_TRANSMISSION_TYPES.intValue()))
            .andExpect(jsonPath("$.cylinder").value(DEFAULT_CYLINDER.intValue()))
            .andExpect(jsonPath("$.tractionTypes").value(DEFAULT_TRACTION_TYPES.intValue()))
            .andExpect(jsonPath("$.bodyWorkTypes").value(DEFAULT_BODY_WORK_TYPES.intValue()))
            .andExpect(jsonPath("$.serviceTypes").value(DEFAULT_SERVICE_TYPES.intValue()))
            .andExpect(jsonPath("$.engineCylindres").value(DEFAULT_ENGINE_CYLINDRES.intValue()))
            .andExpect(jsonPath("$.steeringTypes").value(DEFAULT_STEERING_TYPES.intValue()))
            .andExpect(jsonPath("$.transmissionSpeeds").value(DEFAULT_TRANSMISSION_SPEEDS.intValue()))
            .andExpect(jsonPath("$.breakAssistanceTypes").value(DEFAULT_BREAK_ASSISTANCE_TYPES.intValue()))
            .andExpect(jsonPath("$.airbagTypes").value(DEFAULT_AIRBAG_TYPES.intValue()))
            .andExpect(jsonPath("$.airConditionicTypes").value(DEFAULT_AIR_CONDITIONIC_TYPES.intValue()))
            .andExpect(jsonPath("$.airConditionerControlTypes").value(DEFAULT_AIR_CONDITIONER_CONTROL_TYPES.intValue()))
            .andExpect(jsonPath("$.upholsteryTypes").value(DEFAULT_UPHOLSTERY_TYPES.intValue()))
            .andExpect(jsonPath("$.windowsSystemTypes").value(DEFAULT_WINDOWS_SYSTEM_TYPES.intValue()))
            .andExpect(jsonPath("$.sunroofTypes").value(DEFAULT_SUNROOF_TYPES.intValue()))
            .andExpect(jsonPath("$.financing").value(DEFAULT_FINANCING.toString()))
            .andExpect(jsonPath("$.onlyOwner").value(DEFAULT_ONLY_OWNER.toString()))
            .andExpect(jsonPath("$.negotiable").value(DEFAULT_NEGOTIABLE.toString()))
            .andExpect(jsonPath("$.warrantySellerUntil").value(DEFAULT_WARRANTY_SELLER_UNTIL.toString()))
            .andExpect(jsonPath("$.warrantyConcessionareUntil").value(DEFAULT_WARRANTY_CONCESSIONARE_UNTIL.toString()))
            .andExpect(jsonPath("$.soatCurrencyUpTo").value(DEFAULT_SOAT_CURRENCY_UP_TO.toString()))
            .andExpect(jsonPath("$.tecnoRevCurrentUpTo").value(DEFAULT_TECNO_REV_CURRENT_UP_TO.toString()))
            .andExpect(jsonPath("$.textDescription").value(DEFAULT_TEXT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPublications() throws Exception {
        // Get the publications
        restPublicationsMockMvc.perform(get("/api/publications/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePublications() throws Exception {
        // Initialize the database
        publicationsRepository.saveAndFlush(publications);
        int databaseSizeBeforeUpdate = publicationsRepository.findAll().size();

        // Update the publications
        Publications updatedPublications = publicationsRepository.findOne(publications.getId());
        updatedPublications
            .user(UPDATED_USER)
            .creationDate(UPDATED_CREATION_DATE)
            .lastModifyDate(UPDATED_LAST_MODIFY_DATE)
            .vehicleStatus(UPDATED_VEHICLE_STATUS)
            .specificsCar(UPDATED_SPECIFICS_CAR)
            .carLicense(UPDATED_CAR_LICENSE)
            .platePublicationTypes(UPDATED_PLATE_PUBLICATION_TYPES)
            .ownerDocumentTypes(UPDATED_OWNER_DOCUMENT_TYPES)
            .identificationNumber(UPDATED_IDENTIFICATION_NUMBER)
            .mileage(UPDATED_MILEAGE)
            .hours(UPDATED_HOURS)
            .price(UPDATED_PRICE)
            .colors(UPDATED_COLORS)
            .fuelTypes(UPDATED_FUEL_TYPES)
            .transmissionTypes(UPDATED_TRANSMISSION_TYPES)
            .cylinder(UPDATED_CYLINDER)
            .tractionTypes(UPDATED_TRACTION_TYPES)
            .bodyWorkTypes(UPDATED_BODY_WORK_TYPES)
            .serviceTypes(UPDATED_SERVICE_TYPES)
            .engineCylindres(UPDATED_ENGINE_CYLINDRES)
            .steeringTypes(UPDATED_STEERING_TYPES)
            .transmissionSpeeds(UPDATED_TRANSMISSION_SPEEDS)
            .breakAssistanceTypes(UPDATED_BREAK_ASSISTANCE_TYPES)
            .airbagTypes(UPDATED_AIRBAG_TYPES)
            .airConditionicTypes(UPDATED_AIR_CONDITIONIC_TYPES)
            .airConditionerControlTypes(UPDATED_AIR_CONDITIONER_CONTROL_TYPES)
            .upholsteryTypes(UPDATED_UPHOLSTERY_TYPES)
            .windowsSystemTypes(UPDATED_WINDOWS_SYSTEM_TYPES)
            .sunroofTypes(UPDATED_SUNROOF_TYPES)
            .financing(UPDATED_FINANCING)
            .onlyOwner(UPDATED_ONLY_OWNER)
            .negotiable(UPDATED_NEGOTIABLE)
            .warrantySellerUntil(UPDATED_WARRANTY_SELLER_UNTIL)
            .warrantyConcessionareUntil(UPDATED_WARRANTY_CONCESSIONARE_UNTIL)
            .soatCurrencyUpTo(UPDATED_SOAT_CURRENCY_UP_TO)
            .tecnoRevCurrentUpTo(UPDATED_TECNO_REV_CURRENT_UP_TO)
            .textDescription(UPDATED_TEXT_DESCRIPTION);

        restPublicationsMockMvc.perform(put("/api/publications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPublications)))
            .andExpect(status().isOk());

        // Validate the Publications in the database
        List<Publications> publicationsList = publicationsRepository.findAll();
        assertThat(publicationsList).hasSize(databaseSizeBeforeUpdate);
        Publications testPublications = publicationsList.get(publicationsList.size() - 1);
        assertThat(testPublications.getUser()).isEqualTo(UPDATED_USER);
        assertThat(testPublications.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testPublications.getLastModifyDate()).isEqualTo(UPDATED_LAST_MODIFY_DATE);
        assertThat(testPublications.getVehicleStatus()).isEqualTo(UPDATED_VEHICLE_STATUS);
        assertThat(testPublications.getSpecificsCar()).isEqualTo(UPDATED_SPECIFICS_CAR);
        assertThat(testPublications.getCarLicense()).isEqualTo(UPDATED_CAR_LICENSE);
        assertThat(testPublications.getPlatePublicationTypes()).isEqualTo(UPDATED_PLATE_PUBLICATION_TYPES);
        assertThat(testPublications.getOwnerDocumentTypes()).isEqualTo(UPDATED_OWNER_DOCUMENT_TYPES);
        assertThat(testPublications.getIdentificationNumber()).isEqualTo(UPDATED_IDENTIFICATION_NUMBER);
        assertThat(testPublications.getMileage()).isEqualTo(UPDATED_MILEAGE);
        assertThat(testPublications.getHours()).isEqualTo(UPDATED_HOURS);
        assertThat(testPublications.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testPublications.getColors()).isEqualTo(UPDATED_COLORS);
        assertThat(testPublications.getFuelTypes()).isEqualTo(UPDATED_FUEL_TYPES);
        assertThat(testPublications.getTransmissionTypes()).isEqualTo(UPDATED_TRANSMISSION_TYPES);
        assertThat(testPublications.getCylinder()).isEqualTo(UPDATED_CYLINDER);
        assertThat(testPublications.getTractionTypes()).isEqualTo(UPDATED_TRACTION_TYPES);
        assertThat(testPublications.getBodyWorkTypes()).isEqualTo(UPDATED_BODY_WORK_TYPES);
        assertThat(testPublications.getServiceTypes()).isEqualTo(UPDATED_SERVICE_TYPES);
        assertThat(testPublications.getEngineCylindres()).isEqualTo(UPDATED_ENGINE_CYLINDRES);
        assertThat(testPublications.getSteeringTypes()).isEqualTo(UPDATED_STEERING_TYPES);
        assertThat(testPublications.getTransmissionSpeeds()).isEqualTo(UPDATED_TRANSMISSION_SPEEDS);
        assertThat(testPublications.getBreakAssistanceTypes()).isEqualTo(UPDATED_BREAK_ASSISTANCE_TYPES);
        assertThat(testPublications.getAirbagTypes()).isEqualTo(UPDATED_AIRBAG_TYPES);
        assertThat(testPublications.getAirConditionicTypes()).isEqualTo(UPDATED_AIR_CONDITIONIC_TYPES);
        assertThat(testPublications.getAirConditionerControlTypes()).isEqualTo(UPDATED_AIR_CONDITIONER_CONTROL_TYPES);
        assertThat(testPublications.getUpholsteryTypes()).isEqualTo(UPDATED_UPHOLSTERY_TYPES);
        assertThat(testPublications.getWindowsSystemTypes()).isEqualTo(UPDATED_WINDOWS_SYSTEM_TYPES);
        assertThat(testPublications.getSunroofTypes()).isEqualTo(UPDATED_SUNROOF_TYPES);
        assertThat(testPublications.getFinancing()).isEqualTo(UPDATED_FINANCING);
        assertThat(testPublications.getOnlyOwner()).isEqualTo(UPDATED_ONLY_OWNER);
        assertThat(testPublications.getNegotiable()).isEqualTo(UPDATED_NEGOTIABLE);
        assertThat(testPublications.getWarrantySellerUntil()).isEqualTo(UPDATED_WARRANTY_SELLER_UNTIL);
        assertThat(testPublications.getWarrantyConcessionareUntil()).isEqualTo(UPDATED_WARRANTY_CONCESSIONARE_UNTIL);
        assertThat(testPublications.getSoatCurrencyUpTo()).isEqualTo(UPDATED_SOAT_CURRENCY_UP_TO);
        assertThat(testPublications.getTecnoRevCurrentUpTo()).isEqualTo(UPDATED_TECNO_REV_CURRENT_UP_TO);
        assertThat(testPublications.getTextDescription()).isEqualTo(UPDATED_TEXT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingPublications() throws Exception {
        int databaseSizeBeforeUpdate = publicationsRepository.findAll().size();

        // Create the Publications

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPublicationsMockMvc.perform(put("/api/publications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(publications)))
            .andExpect(status().isCreated());

        // Validate the Publications in the database
        List<Publications> publicationsList = publicationsRepository.findAll();
        assertThat(publicationsList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePublications() throws Exception {
        // Initialize the database
        publicationsRepository.saveAndFlush(publications);
        int databaseSizeBeforeDelete = publicationsRepository.findAll().size();

        // Get the publications
        restPublicationsMockMvc.perform(delete("/api/publications/{id}", publications.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Publications> publicationsList = publicationsRepository.findAll();
        assertThat(publicationsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Publications.class);
        Publications publications1 = new Publications();
        publications1.setId(1L);
        Publications publications2 = new Publications();
        publications2.setId(publications1.getId());
        assertThat(publications1).isEqualTo(publications2);
        publications2.setId(2L);
        assertThat(publications1).isNotEqualTo(publications2);
        publications1.setId(null);
        assertThat(publications1).isNotEqualTo(publications2);
    }
}
