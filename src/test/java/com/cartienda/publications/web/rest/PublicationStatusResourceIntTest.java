package com.cartienda.publications.web.rest;

import com.cartienda.publications.PublicationsApp;

import com.cartienda.publications.domain.PublicationStatus;
import com.cartienda.publications.repository.PublicationStatusRepository;
import com.cartienda.publications.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PublicationStatusResource REST controller.
 *
 * @see PublicationStatusResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PublicationsApp.class)
public class PublicationStatusResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private PublicationStatusRepository publicationStatusRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPublicationStatusMockMvc;

    private PublicationStatus publicationStatus;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PublicationStatusResource publicationStatusResource = new PublicationStatusResource(publicationStatusRepository);
        this.restPublicationStatusMockMvc = MockMvcBuilders.standaloneSetup(publicationStatusResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PublicationStatus createEntity(EntityManager em) {
        PublicationStatus publicationStatus = new PublicationStatus()
            .description(DEFAULT_DESCRIPTION);
        return publicationStatus;
    }

    @Before
    public void initTest() {
        publicationStatus = createEntity(em);
    }

    @Test
    @Transactional
    public void createPublicationStatus() throws Exception {
        int databaseSizeBeforeCreate = publicationStatusRepository.findAll().size();

        // Create the PublicationStatus
        restPublicationStatusMockMvc.perform(post("/api/publication-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(publicationStatus)))
            .andExpect(status().isCreated());

        // Validate the PublicationStatus in the database
        List<PublicationStatus> publicationStatusList = publicationStatusRepository.findAll();
        assertThat(publicationStatusList).hasSize(databaseSizeBeforeCreate + 1);
        PublicationStatus testPublicationStatus = publicationStatusList.get(publicationStatusList.size() - 1);
        assertThat(testPublicationStatus.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createPublicationStatusWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = publicationStatusRepository.findAll().size();

        // Create the PublicationStatus with an existing ID
        publicationStatus.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPublicationStatusMockMvc.perform(post("/api/publication-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(publicationStatus)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<PublicationStatus> publicationStatusList = publicationStatusRepository.findAll();
        assertThat(publicationStatusList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = publicationStatusRepository.findAll().size();
        // set the field null
        publicationStatus.setDescription(null);

        // Create the PublicationStatus, which fails.

        restPublicationStatusMockMvc.perform(post("/api/publication-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(publicationStatus)))
            .andExpect(status().isBadRequest());

        List<PublicationStatus> publicationStatusList = publicationStatusRepository.findAll();
        assertThat(publicationStatusList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPublicationStatuses() throws Exception {
        // Initialize the database
        publicationStatusRepository.saveAndFlush(publicationStatus);

        // Get all the publicationStatusList
        restPublicationStatusMockMvc.perform(get("/api/publication-statuses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(publicationStatus.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getPublicationStatus() throws Exception {
        // Initialize the database
        publicationStatusRepository.saveAndFlush(publicationStatus);

        // Get the publicationStatus
        restPublicationStatusMockMvc.perform(get("/api/publication-statuses/{id}", publicationStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(publicationStatus.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPublicationStatus() throws Exception {
        // Get the publicationStatus
        restPublicationStatusMockMvc.perform(get("/api/publication-statuses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePublicationStatus() throws Exception {
        // Initialize the database
        publicationStatusRepository.saveAndFlush(publicationStatus);
        int databaseSizeBeforeUpdate = publicationStatusRepository.findAll().size();

        // Update the publicationStatus
        PublicationStatus updatedPublicationStatus = publicationStatusRepository.findOne(publicationStatus.getId());
        updatedPublicationStatus
            .description(UPDATED_DESCRIPTION);

        restPublicationStatusMockMvc.perform(put("/api/publication-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPublicationStatus)))
            .andExpect(status().isOk());

        // Validate the PublicationStatus in the database
        List<PublicationStatus> publicationStatusList = publicationStatusRepository.findAll();
        assertThat(publicationStatusList).hasSize(databaseSizeBeforeUpdate);
        PublicationStatus testPublicationStatus = publicationStatusList.get(publicationStatusList.size() - 1);
        assertThat(testPublicationStatus.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingPublicationStatus() throws Exception {
        int databaseSizeBeforeUpdate = publicationStatusRepository.findAll().size();

        // Create the PublicationStatus

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPublicationStatusMockMvc.perform(put("/api/publication-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(publicationStatus)))
            .andExpect(status().isCreated());

        // Validate the PublicationStatus in the database
        List<PublicationStatus> publicationStatusList = publicationStatusRepository.findAll();
        assertThat(publicationStatusList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePublicationStatus() throws Exception {
        // Initialize the database
        publicationStatusRepository.saveAndFlush(publicationStatus);
        int databaseSizeBeforeDelete = publicationStatusRepository.findAll().size();

        // Get the publicationStatus
        restPublicationStatusMockMvc.perform(delete("/api/publication-statuses/{id}", publicationStatus.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PublicationStatus> publicationStatusList = publicationStatusRepository.findAll();
        assertThat(publicationStatusList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PublicationStatus.class);
        PublicationStatus publicationStatus1 = new PublicationStatus();
        publicationStatus1.setId(1L);
        PublicationStatus publicationStatus2 = new PublicationStatus();
        publicationStatus2.setId(publicationStatus1.getId());
        assertThat(publicationStatus1).isEqualTo(publicationStatus2);
        publicationStatus2.setId(2L);
        assertThat(publicationStatus1).isNotEqualTo(publicationStatus2);
        publicationStatus1.setId(null);
        assertThat(publicationStatus1).isNotEqualTo(publicationStatus2);
    }
}
