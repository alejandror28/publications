package com.cartienda.publications.web.rest;

import com.cartienda.publications.PublicationsApp;

import com.cartienda.publications.domain.Accesorios;
import com.cartienda.publications.repository.AccesoriosRepository;
import com.cartienda.publications.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AccesoriosResource REST controller.
 *
 * @see AccesoriosResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PublicationsApp.class)
public class AccesoriosResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private AccesoriosRepository accesoriosRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAccesoriosMockMvc;

    private Accesorios accesorios;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AccesoriosResource accesoriosResource = new AccesoriosResource(accesoriosRepository);
        this.restAccesoriosMockMvc = MockMvcBuilders.standaloneSetup(accesoriosResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Accesorios createEntity(EntityManager em) {
        Accesorios accesorios = new Accesorios()
            .description(DEFAULT_DESCRIPTION);
        return accesorios;
    }

    @Before
    public void initTest() {
        accesorios = createEntity(em);
    }

    @Test
    @Transactional
    public void createAccesorios() throws Exception {
        int databaseSizeBeforeCreate = accesoriosRepository.findAll().size();

        // Create the Accesorios
        restAccesoriosMockMvc.perform(post("/api/accesorios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accesorios)))
            .andExpect(status().isCreated());

        // Validate the Accesorios in the database
        List<Accesorios> accesoriosList = accesoriosRepository.findAll();
        assertThat(accesoriosList).hasSize(databaseSizeBeforeCreate + 1);
        Accesorios testAccesorios = accesoriosList.get(accesoriosList.size() - 1);
        assertThat(testAccesorios.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createAccesoriosWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = accesoriosRepository.findAll().size();

        // Create the Accesorios with an existing ID
        accesorios.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAccesoriosMockMvc.perform(post("/api/accesorios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accesorios)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Accesorios> accesoriosList = accesoriosRepository.findAll();
        assertThat(accesoriosList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = accesoriosRepository.findAll().size();
        // set the field null
        accesorios.setDescription(null);

        // Create the Accesorios, which fails.

        restAccesoriosMockMvc.perform(post("/api/accesorios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accesorios)))
            .andExpect(status().isBadRequest());

        List<Accesorios> accesoriosList = accesoriosRepository.findAll();
        assertThat(accesoriosList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAccesorios() throws Exception {
        // Initialize the database
        accesoriosRepository.saveAndFlush(accesorios);

        // Get all the accesoriosList
        restAccesoriosMockMvc.perform(get("/api/accesorios?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(accesorios.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getAccesorios() throws Exception {
        // Initialize the database
        accesoriosRepository.saveAndFlush(accesorios);

        // Get the accesorios
        restAccesoriosMockMvc.perform(get("/api/accesorios/{id}", accesorios.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(accesorios.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAccesorios() throws Exception {
        // Get the accesorios
        restAccesoriosMockMvc.perform(get("/api/accesorios/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAccesorios() throws Exception {
        // Initialize the database
        accesoriosRepository.saveAndFlush(accesorios);
        int databaseSizeBeforeUpdate = accesoriosRepository.findAll().size();

        // Update the accesorios
        Accesorios updatedAccesorios = accesoriosRepository.findOne(accesorios.getId());
        updatedAccesorios
            .description(UPDATED_DESCRIPTION);

        restAccesoriosMockMvc.perform(put("/api/accesorios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAccesorios)))
            .andExpect(status().isOk());

        // Validate the Accesorios in the database
        List<Accesorios> accesoriosList = accesoriosRepository.findAll();
        assertThat(accesoriosList).hasSize(databaseSizeBeforeUpdate);
        Accesorios testAccesorios = accesoriosList.get(accesoriosList.size() - 1);
        assertThat(testAccesorios.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingAccesorios() throws Exception {
        int databaseSizeBeforeUpdate = accesoriosRepository.findAll().size();

        // Create the Accesorios

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAccesoriosMockMvc.perform(put("/api/accesorios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accesorios)))
            .andExpect(status().isCreated());

        // Validate the Accesorios in the database
        List<Accesorios> accesoriosList = accesoriosRepository.findAll();
        assertThat(accesoriosList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAccesorios() throws Exception {
        // Initialize the database
        accesoriosRepository.saveAndFlush(accesorios);
        int databaseSizeBeforeDelete = accesoriosRepository.findAll().size();

        // Get the accesorios
        restAccesoriosMockMvc.perform(delete("/api/accesorios/{id}", accesorios.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Accesorios> accesoriosList = accesoriosRepository.findAll();
        assertThat(accesoriosList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Accesorios.class);
        Accesorios accesorios1 = new Accesorios();
        accesorios1.setId(1L);
        Accesorios accesorios2 = new Accesorios();
        accesorios2.setId(accesorios1.getId());
        assertThat(accesorios1).isEqualTo(accesorios2);
        accesorios2.setId(2L);
        assertThat(accesorios1).isNotEqualTo(accesorios2);
        accesorios1.setId(null);
        assertThat(accesorios1).isNotEqualTo(accesorios2);
    }
}
