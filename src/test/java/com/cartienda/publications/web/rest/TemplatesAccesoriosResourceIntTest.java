package com.cartienda.publications.web.rest;

import com.cartienda.publications.PublicationsApp;

import com.cartienda.publications.domain.TemplatesAccesorios;
import com.cartienda.publications.repository.TemplatesAccesoriosRepository;
import com.cartienda.publications.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TemplatesAccesoriosResource REST controller.
 *
 * @see TemplatesAccesoriosResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PublicationsApp.class)
public class TemplatesAccesoriosResourceIntTest {

    @Autowired
    private TemplatesAccesoriosRepository templatesAccesoriosRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTemplatesAccesoriosMockMvc;

    private TemplatesAccesorios templatesAccesorios;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TemplatesAccesoriosResource templatesAccesoriosResource = new TemplatesAccesoriosResource(templatesAccesoriosRepository);
        this.restTemplatesAccesoriosMockMvc = MockMvcBuilders.standaloneSetup(templatesAccesoriosResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TemplatesAccesorios createEntity(EntityManager em) {
        TemplatesAccesorios templatesAccesorios = new TemplatesAccesorios();
        return templatesAccesorios;
    }

    @Before
    public void initTest() {
        templatesAccesorios = createEntity(em);
    }

    @Test
    @Transactional
    public void createTemplatesAccesorios() throws Exception {
        int databaseSizeBeforeCreate = templatesAccesoriosRepository.findAll().size();

        // Create the TemplatesAccesorios
        restTemplatesAccesoriosMockMvc.perform(post("/api/templates-accesorios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(templatesAccesorios)))
            .andExpect(status().isCreated());

        // Validate the TemplatesAccesorios in the database
        List<TemplatesAccesorios> templatesAccesoriosList = templatesAccesoriosRepository.findAll();
        assertThat(templatesAccesoriosList).hasSize(databaseSizeBeforeCreate + 1);
        TemplatesAccesorios testTemplatesAccesorios = templatesAccesoriosList.get(templatesAccesoriosList.size() - 1);
    }

    @Test
    @Transactional
    public void createTemplatesAccesoriosWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = templatesAccesoriosRepository.findAll().size();

        // Create the TemplatesAccesorios with an existing ID
        templatesAccesorios.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTemplatesAccesoriosMockMvc.perform(post("/api/templates-accesorios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(templatesAccesorios)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<TemplatesAccesorios> templatesAccesoriosList = templatesAccesoriosRepository.findAll();
        assertThat(templatesAccesoriosList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTemplatesAccesorios() throws Exception {
        // Initialize the database
        templatesAccesoriosRepository.saveAndFlush(templatesAccesorios);

        // Get all the templatesAccesoriosList
        restTemplatesAccesoriosMockMvc.perform(get("/api/templates-accesorios?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(templatesAccesorios.getId().intValue())));
    }

    @Test
    @Transactional
    public void getTemplatesAccesorios() throws Exception {
        // Initialize the database
        templatesAccesoriosRepository.saveAndFlush(templatesAccesorios);

        // Get the templatesAccesorios
        restTemplatesAccesoriosMockMvc.perform(get("/api/templates-accesorios/{id}", templatesAccesorios.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(templatesAccesorios.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingTemplatesAccesorios() throws Exception {
        // Get the templatesAccesorios
        restTemplatesAccesoriosMockMvc.perform(get("/api/templates-accesorios/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTemplatesAccesorios() throws Exception {
        // Initialize the database
        templatesAccesoriosRepository.saveAndFlush(templatesAccesorios);
        int databaseSizeBeforeUpdate = templatesAccesoriosRepository.findAll().size();

        // Update the templatesAccesorios
        TemplatesAccesorios updatedTemplatesAccesorios = templatesAccesoriosRepository.findOne(templatesAccesorios.getId());

        restTemplatesAccesoriosMockMvc.perform(put("/api/templates-accesorios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTemplatesAccesorios)))
            .andExpect(status().isOk());

        // Validate the TemplatesAccesorios in the database
        List<TemplatesAccesorios> templatesAccesoriosList = templatesAccesoriosRepository.findAll();
        assertThat(templatesAccesoriosList).hasSize(databaseSizeBeforeUpdate);
        TemplatesAccesorios testTemplatesAccesorios = templatesAccesoriosList.get(templatesAccesoriosList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingTemplatesAccesorios() throws Exception {
        int databaseSizeBeforeUpdate = templatesAccesoriosRepository.findAll().size();

        // Create the TemplatesAccesorios

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTemplatesAccesoriosMockMvc.perform(put("/api/templates-accesorios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(templatesAccesorios)))
            .andExpect(status().isCreated());

        // Validate the TemplatesAccesorios in the database
        List<TemplatesAccesorios> templatesAccesoriosList = templatesAccesoriosRepository.findAll();
        assertThat(templatesAccesoriosList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTemplatesAccesorios() throws Exception {
        // Initialize the database
        templatesAccesoriosRepository.saveAndFlush(templatesAccesorios);
        int databaseSizeBeforeDelete = templatesAccesoriosRepository.findAll().size();

        // Get the templatesAccesorios
        restTemplatesAccesoriosMockMvc.perform(delete("/api/templates-accesorios/{id}", templatesAccesorios.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TemplatesAccesorios> templatesAccesoriosList = templatesAccesoriosRepository.findAll();
        assertThat(templatesAccesoriosList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TemplatesAccesorios.class);
        TemplatesAccesorios templatesAccesorios1 = new TemplatesAccesorios();
        templatesAccesorios1.setId(1L);
        TemplatesAccesorios templatesAccesorios2 = new TemplatesAccesorios();
        templatesAccesorios2.setId(templatesAccesorios1.getId());
        assertThat(templatesAccesorios1).isEqualTo(templatesAccesorios2);
        templatesAccesorios2.setId(2L);
        assertThat(templatesAccesorios1).isNotEqualTo(templatesAccesorios2);
        templatesAccesorios1.setId(null);
        assertThat(templatesAccesorios1).isNotEqualTo(templatesAccesorios2);
    }
}
