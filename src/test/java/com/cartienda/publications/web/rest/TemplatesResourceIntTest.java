package com.cartienda.publications.web.rest;

import com.cartienda.publications.PublicationsApp;

import com.cartienda.publications.domain.Templates;
import com.cartienda.publications.repository.TemplatesRepository;
import com.cartienda.publications.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TemplatesResource REST controller.
 *
 * @see TemplatesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PublicationsApp.class)
public class TemplatesResourceIntTest {

    private static final Long DEFAULT_SPECIFICS_CAR = 1L;
    private static final Long UPDATED_SPECIFICS_CAR = 2L;

    private static final Long DEFAULT_COLORS = 1L;
    private static final Long UPDATED_COLORS = 2L;

    private static final Long DEFAULT_FUEL_TYPES = 1L;
    private static final Long UPDATED_FUEL_TYPES = 2L;

    private static final Long DEFAULT_TRANSMISSION_TYPES = 1L;
    private static final Long UPDATED_TRANSMISSION_TYPES = 2L;

    private static final Long DEFAULT_CYLINDER = 1L;
    private static final Long UPDATED_CYLINDER = 2L;

    private static final Long DEFAULT_TRACTION_TYPES = 1L;
    private static final Long UPDATED_TRACTION_TYPES = 2L;

    private static final Long DEFAULT_BODY_WORK_TYPES = 1L;
    private static final Long UPDATED_BODY_WORK_TYPES = 2L;

    private static final Long DEFAULT_SERVICE_TYPES = 1L;
    private static final Long UPDATED_SERVICE_TYPES = 2L;

    private static final Long DEFAULT_ENGINE_CYLINDRES = 1L;
    private static final Long UPDATED_ENGINE_CYLINDRES = 2L;

    private static final Long DEFAULT_STEERING_TYPES = 1L;
    private static final Long UPDATED_STEERING_TYPES = 2L;

    private static final Long DEFAULT_TRANSMISSION_SPEEDS = 1L;
    private static final Long UPDATED_TRANSMISSION_SPEEDS = 2L;

    private static final Long DEFAULT_BREAK_ASSISTANCE_TYPES = 1L;
    private static final Long UPDATED_BREAK_ASSISTANCE_TYPES = 2L;

    private static final Long DEFAULT_AIRBAG_TYPES = 1L;
    private static final Long UPDATED_AIRBAG_TYPES = 2L;

    private static final Long DEFAULT_AIR_CONDITIONIC_TYPES = 1L;
    private static final Long UPDATED_AIR_CONDITIONIC_TYPES = 2L;

    private static final Long DEFAULT_AIR_CONDITIONER_CONTROL_TYPES = 1L;
    private static final Long UPDATED_AIR_CONDITIONER_CONTROL_TYPES = 2L;

    private static final Long DEFAULT_UPHOLSTERY_TYPES = 1L;
    private static final Long UPDATED_UPHOLSTERY_TYPES = 2L;

    private static final Long DEFAULT_WINDOWS_SYSTEM_TYPES = 1L;
    private static final Long UPDATED_WINDOWS_SYSTEM_TYPES = 2L;

    private static final Long DEFAULT_SUNROOF_TYPES = 1L;
    private static final Long UPDATED_SUNROOF_TYPES = 2L;

    @Autowired
    private TemplatesRepository templatesRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTemplatesMockMvc;

    private Templates templates;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TemplatesResource templatesResource = new TemplatesResource(templatesRepository);
        this.restTemplatesMockMvc = MockMvcBuilders.standaloneSetup(templatesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Templates createEntity(EntityManager em) {
        Templates templates = new Templates()
            .specificsCar(DEFAULT_SPECIFICS_CAR)
            .colors(DEFAULT_COLORS)
            .fuelTypes(DEFAULT_FUEL_TYPES)
            .transmissionTypes(DEFAULT_TRANSMISSION_TYPES)
            .cylinder(DEFAULT_CYLINDER)
            .tractionTypes(DEFAULT_TRACTION_TYPES)
            .bodyWorkTypes(DEFAULT_BODY_WORK_TYPES)
            .serviceTypes(DEFAULT_SERVICE_TYPES)
            .engineCylindres(DEFAULT_ENGINE_CYLINDRES)
            .steeringTypes(DEFAULT_STEERING_TYPES)
            .transmissionSpeeds(DEFAULT_TRANSMISSION_SPEEDS)
            .breakAssistanceTypes(DEFAULT_BREAK_ASSISTANCE_TYPES)
            .airbagTypes(DEFAULT_AIRBAG_TYPES)
            .airConditionicTypes(DEFAULT_AIR_CONDITIONIC_TYPES)
            .airConditionerControlTypes(DEFAULT_AIR_CONDITIONER_CONTROL_TYPES)
            .upholsteryTypes(DEFAULT_UPHOLSTERY_TYPES)
            .windowsSystemTypes(DEFAULT_WINDOWS_SYSTEM_TYPES)
            .sunroofTypes(DEFAULT_SUNROOF_TYPES);
        return templates;
    }

    @Before
    public void initTest() {
        templates = createEntity(em);
    }

    @Test
    @Transactional
    public void createTemplates() throws Exception {
        int databaseSizeBeforeCreate = templatesRepository.findAll().size();

        // Create the Templates
        restTemplatesMockMvc.perform(post("/api/templates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(templates)))
            .andExpect(status().isCreated());

        // Validate the Templates in the database
        List<Templates> templatesList = templatesRepository.findAll();
        assertThat(templatesList).hasSize(databaseSizeBeforeCreate + 1);
        Templates testTemplates = templatesList.get(templatesList.size() - 1);
        assertThat(testTemplates.getSpecificsCar()).isEqualTo(DEFAULT_SPECIFICS_CAR);
        assertThat(testTemplates.getColors()).isEqualTo(DEFAULT_COLORS);
        assertThat(testTemplates.getFuelTypes()).isEqualTo(DEFAULT_FUEL_TYPES);
        assertThat(testTemplates.getTransmissionTypes()).isEqualTo(DEFAULT_TRANSMISSION_TYPES);
        assertThat(testTemplates.getCylinder()).isEqualTo(DEFAULT_CYLINDER);
        assertThat(testTemplates.getTractionTypes()).isEqualTo(DEFAULT_TRACTION_TYPES);
        assertThat(testTemplates.getBodyWorkTypes()).isEqualTo(DEFAULT_BODY_WORK_TYPES);
        assertThat(testTemplates.getServiceTypes()).isEqualTo(DEFAULT_SERVICE_TYPES);
        assertThat(testTemplates.getEngineCylindres()).isEqualTo(DEFAULT_ENGINE_CYLINDRES);
        assertThat(testTemplates.getSteeringTypes()).isEqualTo(DEFAULT_STEERING_TYPES);
        assertThat(testTemplates.getTransmissionSpeeds()).isEqualTo(DEFAULT_TRANSMISSION_SPEEDS);
        assertThat(testTemplates.getBreakAssistanceTypes()).isEqualTo(DEFAULT_BREAK_ASSISTANCE_TYPES);
        assertThat(testTemplates.getAirbagTypes()).isEqualTo(DEFAULT_AIRBAG_TYPES);
        assertThat(testTemplates.getAirConditionicTypes()).isEqualTo(DEFAULT_AIR_CONDITIONIC_TYPES);
        assertThat(testTemplates.getAirConditionerControlTypes()).isEqualTo(DEFAULT_AIR_CONDITIONER_CONTROL_TYPES);
        assertThat(testTemplates.getUpholsteryTypes()).isEqualTo(DEFAULT_UPHOLSTERY_TYPES);
        assertThat(testTemplates.getWindowsSystemTypes()).isEqualTo(DEFAULT_WINDOWS_SYSTEM_TYPES);
        assertThat(testTemplates.getSunroofTypes()).isEqualTo(DEFAULT_SUNROOF_TYPES);
    }

    @Test
    @Transactional
    public void createTemplatesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = templatesRepository.findAll().size();

        // Create the Templates with an existing ID
        templates.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTemplatesMockMvc.perform(post("/api/templates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(templates)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Templates> templatesList = templatesRepository.findAll();
        assertThat(templatesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTemplates() throws Exception {
        // Initialize the database
        templatesRepository.saveAndFlush(templates);

        // Get all the templatesList
        restTemplatesMockMvc.perform(get("/api/templates?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(templates.getId().intValue())))
            .andExpect(jsonPath("$.[*].specificsCar").value(hasItem(DEFAULT_SPECIFICS_CAR.intValue())))
            .andExpect(jsonPath("$.[*].colors").value(hasItem(DEFAULT_COLORS.intValue())))
            .andExpect(jsonPath("$.[*].fuelTypes").value(hasItem(DEFAULT_FUEL_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].transmissionTypes").value(hasItem(DEFAULT_TRANSMISSION_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].cylinder").value(hasItem(DEFAULT_CYLINDER.intValue())))
            .andExpect(jsonPath("$.[*].tractionTypes").value(hasItem(DEFAULT_TRACTION_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].bodyWorkTypes").value(hasItem(DEFAULT_BODY_WORK_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].serviceTypes").value(hasItem(DEFAULT_SERVICE_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].engineCylindres").value(hasItem(DEFAULT_ENGINE_CYLINDRES.intValue())))
            .andExpect(jsonPath("$.[*].steeringTypes").value(hasItem(DEFAULT_STEERING_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].transmissionSpeeds").value(hasItem(DEFAULT_TRANSMISSION_SPEEDS.intValue())))
            .andExpect(jsonPath("$.[*].breakAssistanceTypes").value(hasItem(DEFAULT_BREAK_ASSISTANCE_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].airbagTypes").value(hasItem(DEFAULT_AIRBAG_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].airConditionicTypes").value(hasItem(DEFAULT_AIR_CONDITIONIC_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].airConditionerControlTypes").value(hasItem(DEFAULT_AIR_CONDITIONER_CONTROL_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].upholsteryTypes").value(hasItem(DEFAULT_UPHOLSTERY_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].windowsSystemTypes").value(hasItem(DEFAULT_WINDOWS_SYSTEM_TYPES.intValue())))
            .andExpect(jsonPath("$.[*].sunroofTypes").value(hasItem(DEFAULT_SUNROOF_TYPES.intValue())));
    }

    @Test
    @Transactional
    public void getTemplates() throws Exception {
        // Initialize the database
        templatesRepository.saveAndFlush(templates);

        // Get the templates
        restTemplatesMockMvc.perform(get("/api/templates/{id}", templates.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(templates.getId().intValue()))
            .andExpect(jsonPath("$.specificsCar").value(DEFAULT_SPECIFICS_CAR.intValue()))
            .andExpect(jsonPath("$.colors").value(DEFAULT_COLORS.intValue()))
            .andExpect(jsonPath("$.fuelTypes").value(DEFAULT_FUEL_TYPES.intValue()))
            .andExpect(jsonPath("$.transmissionTypes").value(DEFAULT_TRANSMISSION_TYPES.intValue()))
            .andExpect(jsonPath("$.cylinder").value(DEFAULT_CYLINDER.intValue()))
            .andExpect(jsonPath("$.tractionTypes").value(DEFAULT_TRACTION_TYPES.intValue()))
            .andExpect(jsonPath("$.bodyWorkTypes").value(DEFAULT_BODY_WORK_TYPES.intValue()))
            .andExpect(jsonPath("$.serviceTypes").value(DEFAULT_SERVICE_TYPES.intValue()))
            .andExpect(jsonPath("$.engineCylindres").value(DEFAULT_ENGINE_CYLINDRES.intValue()))
            .andExpect(jsonPath("$.steeringTypes").value(DEFAULT_STEERING_TYPES.intValue()))
            .andExpect(jsonPath("$.transmissionSpeeds").value(DEFAULT_TRANSMISSION_SPEEDS.intValue()))
            .andExpect(jsonPath("$.breakAssistanceTypes").value(DEFAULT_BREAK_ASSISTANCE_TYPES.intValue()))
            .andExpect(jsonPath("$.airbagTypes").value(DEFAULT_AIRBAG_TYPES.intValue()))
            .andExpect(jsonPath("$.airConditionicTypes").value(DEFAULT_AIR_CONDITIONIC_TYPES.intValue()))
            .andExpect(jsonPath("$.airConditionerControlTypes").value(DEFAULT_AIR_CONDITIONER_CONTROL_TYPES.intValue()))
            .andExpect(jsonPath("$.upholsteryTypes").value(DEFAULT_UPHOLSTERY_TYPES.intValue()))
            .andExpect(jsonPath("$.windowsSystemTypes").value(DEFAULT_WINDOWS_SYSTEM_TYPES.intValue()))
            .andExpect(jsonPath("$.sunroofTypes").value(DEFAULT_SUNROOF_TYPES.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingTemplates() throws Exception {
        // Get the templates
        restTemplatesMockMvc.perform(get("/api/templates/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTemplates() throws Exception {
        // Initialize the database
        templatesRepository.saveAndFlush(templates);
        int databaseSizeBeforeUpdate = templatesRepository.findAll().size();

        // Update the templates
        Templates updatedTemplates = templatesRepository.findOne(templates.getId());
        updatedTemplates
            .specificsCar(UPDATED_SPECIFICS_CAR)
            .colors(UPDATED_COLORS)
            .fuelTypes(UPDATED_FUEL_TYPES)
            .transmissionTypes(UPDATED_TRANSMISSION_TYPES)
            .cylinder(UPDATED_CYLINDER)
            .tractionTypes(UPDATED_TRACTION_TYPES)
            .bodyWorkTypes(UPDATED_BODY_WORK_TYPES)
            .serviceTypes(UPDATED_SERVICE_TYPES)
            .engineCylindres(UPDATED_ENGINE_CYLINDRES)
            .steeringTypes(UPDATED_STEERING_TYPES)
            .transmissionSpeeds(UPDATED_TRANSMISSION_SPEEDS)
            .breakAssistanceTypes(UPDATED_BREAK_ASSISTANCE_TYPES)
            .airbagTypes(UPDATED_AIRBAG_TYPES)
            .airConditionicTypes(UPDATED_AIR_CONDITIONIC_TYPES)
            .airConditionerControlTypes(UPDATED_AIR_CONDITIONER_CONTROL_TYPES)
            .upholsteryTypes(UPDATED_UPHOLSTERY_TYPES)
            .windowsSystemTypes(UPDATED_WINDOWS_SYSTEM_TYPES)
            .sunroofTypes(UPDATED_SUNROOF_TYPES);

        restTemplatesMockMvc.perform(put("/api/templates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTemplates)))
            .andExpect(status().isOk());

        // Validate the Templates in the database
        List<Templates> templatesList = templatesRepository.findAll();
        assertThat(templatesList).hasSize(databaseSizeBeforeUpdate);
        Templates testTemplates = templatesList.get(templatesList.size() - 1);
        assertThat(testTemplates.getSpecificsCar()).isEqualTo(UPDATED_SPECIFICS_CAR);
        assertThat(testTemplates.getColors()).isEqualTo(UPDATED_COLORS);
        assertThat(testTemplates.getFuelTypes()).isEqualTo(UPDATED_FUEL_TYPES);
        assertThat(testTemplates.getTransmissionTypes()).isEqualTo(UPDATED_TRANSMISSION_TYPES);
        assertThat(testTemplates.getCylinder()).isEqualTo(UPDATED_CYLINDER);
        assertThat(testTemplates.getTractionTypes()).isEqualTo(UPDATED_TRACTION_TYPES);
        assertThat(testTemplates.getBodyWorkTypes()).isEqualTo(UPDATED_BODY_WORK_TYPES);
        assertThat(testTemplates.getServiceTypes()).isEqualTo(UPDATED_SERVICE_TYPES);
        assertThat(testTemplates.getEngineCylindres()).isEqualTo(UPDATED_ENGINE_CYLINDRES);
        assertThat(testTemplates.getSteeringTypes()).isEqualTo(UPDATED_STEERING_TYPES);
        assertThat(testTemplates.getTransmissionSpeeds()).isEqualTo(UPDATED_TRANSMISSION_SPEEDS);
        assertThat(testTemplates.getBreakAssistanceTypes()).isEqualTo(UPDATED_BREAK_ASSISTANCE_TYPES);
        assertThat(testTemplates.getAirbagTypes()).isEqualTo(UPDATED_AIRBAG_TYPES);
        assertThat(testTemplates.getAirConditionicTypes()).isEqualTo(UPDATED_AIR_CONDITIONIC_TYPES);
        assertThat(testTemplates.getAirConditionerControlTypes()).isEqualTo(UPDATED_AIR_CONDITIONER_CONTROL_TYPES);
        assertThat(testTemplates.getUpholsteryTypes()).isEqualTo(UPDATED_UPHOLSTERY_TYPES);
        assertThat(testTemplates.getWindowsSystemTypes()).isEqualTo(UPDATED_WINDOWS_SYSTEM_TYPES);
        assertThat(testTemplates.getSunroofTypes()).isEqualTo(UPDATED_SUNROOF_TYPES);
    }

    @Test
    @Transactional
    public void updateNonExistingTemplates() throws Exception {
        int databaseSizeBeforeUpdate = templatesRepository.findAll().size();

        // Create the Templates

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTemplatesMockMvc.perform(put("/api/templates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(templates)))
            .andExpect(status().isCreated());

        // Validate the Templates in the database
        List<Templates> templatesList = templatesRepository.findAll();
        assertThat(templatesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTemplates() throws Exception {
        // Initialize the database
        templatesRepository.saveAndFlush(templates);
        int databaseSizeBeforeDelete = templatesRepository.findAll().size();

        // Get the templates
        restTemplatesMockMvc.perform(delete("/api/templates/{id}", templates.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Templates> templatesList = templatesRepository.findAll();
        assertThat(templatesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Templates.class);
        Templates templates1 = new Templates();
        templates1.setId(1L);
        Templates templates2 = new Templates();
        templates2.setId(templates1.getId());
        assertThat(templates1).isEqualTo(templates2);
        templates2.setId(2L);
        assertThat(templates1).isNotEqualTo(templates2);
        templates1.setId(null);
        assertThat(templates1).isNotEqualTo(templates2);
    }
}
