package com.cartienda.publications.web.rest;

import com.cartienda.publications.PublicationsApp;

import com.cartienda.publications.domain.PublicationAccesorios;
import com.cartienda.publications.repository.PublicationAccesoriosRepository;
import com.cartienda.publications.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PublicationAccesoriosResource REST controller.
 *
 * @see PublicationAccesoriosResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PublicationsApp.class)
public class PublicationAccesoriosResourceIntTest {

    @Autowired
    private PublicationAccesoriosRepository publicationAccesoriosRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPublicationAccesoriosMockMvc;

    private PublicationAccesorios publicationAccesorios;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PublicationAccesoriosResource publicationAccesoriosResource = new PublicationAccesoriosResource(publicationAccesoriosRepository);
        this.restPublicationAccesoriosMockMvc = MockMvcBuilders.standaloneSetup(publicationAccesoriosResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PublicationAccesorios createEntity(EntityManager em) {
        PublicationAccesorios publicationAccesorios = new PublicationAccesorios();
        return publicationAccesorios;
    }

    @Before
    public void initTest() {
        publicationAccesorios = createEntity(em);
    }

    @Test
    @Transactional
    public void createPublicationAccesorios() throws Exception {
        int databaseSizeBeforeCreate = publicationAccesoriosRepository.findAll().size();

        // Create the PublicationAccesorios
        restPublicationAccesoriosMockMvc.perform(post("/api/publication-accesorios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(publicationAccesorios)))
            .andExpect(status().isCreated());

        // Validate the PublicationAccesorios in the database
        List<PublicationAccesorios> publicationAccesoriosList = publicationAccesoriosRepository.findAll();
        assertThat(publicationAccesoriosList).hasSize(databaseSizeBeforeCreate + 1);
        PublicationAccesorios testPublicationAccesorios = publicationAccesoriosList.get(publicationAccesoriosList.size() - 1);
    }

    @Test
    @Transactional
    public void createPublicationAccesoriosWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = publicationAccesoriosRepository.findAll().size();

        // Create the PublicationAccesorios with an existing ID
        publicationAccesorios.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPublicationAccesoriosMockMvc.perform(post("/api/publication-accesorios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(publicationAccesorios)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<PublicationAccesorios> publicationAccesoriosList = publicationAccesoriosRepository.findAll();
        assertThat(publicationAccesoriosList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPublicationAccesorios() throws Exception {
        // Initialize the database
        publicationAccesoriosRepository.saveAndFlush(publicationAccesorios);

        // Get all the publicationAccesoriosList
        restPublicationAccesoriosMockMvc.perform(get("/api/publication-accesorios?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(publicationAccesorios.getId().intValue())));
    }

    @Test
    @Transactional
    public void getPublicationAccesorios() throws Exception {
        // Initialize the database
        publicationAccesoriosRepository.saveAndFlush(publicationAccesorios);

        // Get the publicationAccesorios
        restPublicationAccesoriosMockMvc.perform(get("/api/publication-accesorios/{id}", publicationAccesorios.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(publicationAccesorios.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingPublicationAccesorios() throws Exception {
        // Get the publicationAccesorios
        restPublicationAccesoriosMockMvc.perform(get("/api/publication-accesorios/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePublicationAccesorios() throws Exception {
        // Initialize the database
        publicationAccesoriosRepository.saveAndFlush(publicationAccesorios);
        int databaseSizeBeforeUpdate = publicationAccesoriosRepository.findAll().size();

        // Update the publicationAccesorios
        PublicationAccesorios updatedPublicationAccesorios = publicationAccesoriosRepository.findOne(publicationAccesorios.getId());

        restPublicationAccesoriosMockMvc.perform(put("/api/publication-accesorios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPublicationAccesorios)))
            .andExpect(status().isOk());

        // Validate the PublicationAccesorios in the database
        List<PublicationAccesorios> publicationAccesoriosList = publicationAccesoriosRepository.findAll();
        assertThat(publicationAccesoriosList).hasSize(databaseSizeBeforeUpdate);
        PublicationAccesorios testPublicationAccesorios = publicationAccesoriosList.get(publicationAccesoriosList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingPublicationAccesorios() throws Exception {
        int databaseSizeBeforeUpdate = publicationAccesoriosRepository.findAll().size();

        // Create the PublicationAccesorios

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPublicationAccesoriosMockMvc.perform(put("/api/publication-accesorios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(publicationAccesorios)))
            .andExpect(status().isCreated());

        // Validate the PublicationAccesorios in the database
        List<PublicationAccesorios> publicationAccesoriosList = publicationAccesoriosRepository.findAll();
        assertThat(publicationAccesoriosList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePublicationAccesorios() throws Exception {
        // Initialize the database
        publicationAccesoriosRepository.saveAndFlush(publicationAccesorios);
        int databaseSizeBeforeDelete = publicationAccesoriosRepository.findAll().size();

        // Get the publicationAccesorios
        restPublicationAccesoriosMockMvc.perform(delete("/api/publication-accesorios/{id}", publicationAccesorios.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PublicationAccesorios> publicationAccesoriosList = publicationAccesoriosRepository.findAll();
        assertThat(publicationAccesoriosList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PublicationAccesorios.class);
        PublicationAccesorios publicationAccesorios1 = new PublicationAccesorios();
        publicationAccesorios1.setId(1L);
        PublicationAccesorios publicationAccesorios2 = new PublicationAccesorios();
        publicationAccesorios2.setId(publicationAccesorios1.getId());
        assertThat(publicationAccesorios1).isEqualTo(publicationAccesorios2);
        publicationAccesorios2.setId(2L);
        assertThat(publicationAccesorios1).isNotEqualTo(publicationAccesorios2);
        publicationAccesorios1.setId(null);
        assertThat(publicationAccesorios1).isNotEqualTo(publicationAccesorios2);
    }
}
